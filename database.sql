/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.0.45-community-nt : Database - amin_ulla
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`amin_ulla` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `amin_ulla`;

/*Table structure for table `catagory` */

DROP TABLE IF EXISTS `catagory`;

CREATE TABLE `catagory` (
  `ID` int(200) NOT NULL auto_increment,
  `Test_catagory` varchar(200) NOT NULL,
  PRIMARY KEY  (`ID`,`Test_catagory`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `catagory` */

insert  into `catagory`(`ID`,`Test_catagory`) values (1,'hematology & serology'),(2,'biochemical exam'),(3,'serologycal exam'),(4,'microbiology exam'),(5,'urine exam'),(6,'stool exam'),(7,'elisa test'),(8,'x-rey exam'),(10,'ultra sonography 4d color doppler exam'),(11,'x-rey exam :(c.r machin)'),(12,'ultra sonography exam(b&w)'),(13,'hormaones test'),(14,'hepatitis profile'),(15,'tumour markers'),(16,'torch pannel'),(17,'serology'),(18,'biochemical test');

/*Table structure for table `customar` */

DROP TABLE IF EXISTS `customar`;

CREATE TABLE `customar` (
  `Id` int(200) NOT NULL auto_increment,
  `Customar_Name` varchar(200) default NULL,
  `Tests` varchar(200) default NULL,
  `Age` int(200) default NULL,
  `Gendar` varchar(200) default NULL,
  `Mobile` varchar(200) default NULL,
  `Address` varchar(200) default NULL,
  `Dr_Name` varchar(200) default NULL,
  `Date` varchar(200) default NULL,
  `Month_year` date default NULL,
  `Time` varchar(200) default NULL,
  `taka` int(200) default NULL,
  `DisCount` int(200) default NULL,
  `paid` int(200) default NULL,
  `unpaid` int(200) default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `customar` */

insert  into `customar`(`Id`,`Customar_Name`,`Tests`,`Age`,`Gendar`,`Mobile`,`Address`,`Dr_Name`,`Date`,`Month_year`,`Time`,`taka`,`DisCount`,`paid`,`unpaid`) values (1,'Md. Sahidullah','cbc (cell counter),x-ray chest p/a,random blood sugar(rbs),blood for hbs ag (screening),blood for s.bilirubin,creatinine clearance,lipid profile,bt.ct.,hb%,esr,dc,tc,',50,'Male','01815073984','GM Hat','DR.ZAMAN','15-08-2018',NULL,'01:05:07 PM',3750,0,3750,0),(2,'Runa','blood for hbs ag (screening),random blood sugar(rbs),urine for r/e,blood for s.sgpt(alt),cbc (cell counter),',21,'Select Gender','01816352840','M. daliu','Dr.Auab al mamun','15-08-2018',NULL,'04:30:50 PM',1450,0,1450,0),(3,'Rasel','blood for hbs ag (screening),fasting blood sugar (fbs),t3,t4,tsh,urine for r/e,blood for s.bilirubin,blood for sgot(ast),cbc (cell counter),',29,'Male','01814948323','Motigong','Dr.Auab al mamun','15-08-2018',NULL,'04:39:35 PM',3700,0,3700,0),(4,'morzina akter ','duplex study(any),random blood sugar(rbs),urine for r/e,cbc (cell counter),',26,'Female','01811969321','sernbag','Dr.Auab al mamun','15-08-2018',NULL,'08:36:25 PM',2100,0,2100,0),(5,'hosne ara begam','blood for c.r.p,',60,'Female','01869653197','gobindapur','Dr.Auab al mamun','15-08-2018',NULL,'05:30:33 PM',850,0,850,0);

/*Table structure for table `doctort` */

DROP TABLE IF EXISTS `doctort`;

CREATE TABLE `doctort` (
  `id` int(200) NOT NULL auto_increment,
  `Doctor_name` varchar(200) default NULL,
  `Attendant` varchar(200) default NULL,
  `Attendant_time` varchar(200) default NULL,
  `mobile` varchar(200) default NULL,
  `Address` varchar(200) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `doctort` */

insert  into `doctort`(`id`,`Doctor_name`,`Attendant`,`Attendant_time`,`mobile`,`Address`) values (3,'Dr.Sujon Kamur ','sun,mon','10 Am-1PM','2656','Ctg'),(4,'Dr. Sujon kumar dor .MBBS,D-card,CCD','Friday,wed','9-00 am -8-00 pm,4.00pm-8.00pm','',''),(5,'Dr Amin Uddin(Fcps,Mbbs)','sat,mon,wens','4:00pm','56','addsdc'),(8,'DR.ZAMAN','EVRIDAY','t0 TO 12 AM','1711665627','Master Para'),(9,'Dr.Auab al mamun','wednesday','4:00pm-8:00-pm','00000000000','M. dalia'),(10,'Dr.Nasir','Friday','9 am to 2 pm','00000000','ctg medical College'),(11,'Dr.Faisal','Thursday','10 am to 4 pm','111111111','dhaka'),(12,'Dr.Nazmul Hoque','Evreday','10 am to 4 pm','2244444','Feni'),(13,'Dr Toma','Everyday','Whole Day','01688996232','feni Town');

/*Table structure for table `sequrity` */

DROP TABLE IF EXISTS `sequrity`;

CREATE TABLE `sequrity` (
  `Id` int(200) NOT NULL auto_increment,
  `User_Name` varchar(200) default NULL,
  `Password` int(200) default NULL,
  `stap_User_Name` varchar(200) default NULL,
  `stap_Password` int(200) default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `sequrity` */

insert  into `sequrity`(`Id`,`User_Name`,`Password`,`stap_User_Name`,`stap_Password`) values (4,'Azom',27566611,'amc',123),(5,NULL,NULL,'Azom',27566611),(6,NULL,NULL,'susanto',1234);

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `SID` int(200) NOT NULL auto_increment,
  `Test_catagory` varchar(200) default '',
  `Test_name` varchar(200) NOT NULL default '',
  `Test_Price` int(200) default NULL,
  PRIMARY KEY  (`SID`,`Test_name`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

/*Data for the table `test` */

insert  into `test`(`SID`,`Test_catagory`,`Test_name`,`Test_Price`) values (1,'hematology & serology','tc',100),(2,'hematology & serology','dc',100),(3,'hematology & serology','esr',100),(4,'hematology & serology','hb%',100),(5,'hematology & serology','ce',100),(7,'hematology & serology','mp',100),(8,'hematology & serology','platelets count',200),(9,'hematology & serology','bt.ct.',150),(10,'hematology & serology','pcv',100),(11,'hematology & serology','mcv,mch,mchc',250),(12,'hematology & serology','pbf',600),(13,'hematology & serology','l.e.cell',350),(14,'hematology & serology','r.b.c count',200),(15,'hematology & serology','recticulocyte',250),(16,'hematology & serology','ptrothombin time',700),(17,'hematology & serology','mp spot test',500),(18,'hematology & serology','cbc (cell counter)',600),(19,'biochemical exam','hbalc',1000),(20,'biochemical exam','fasting blood sugar (fbs)',100),(21,'biochemical exam','random blood sugar(rbs)',100),(22,'biochemical exam','g.t.t(3 sample)',300),(23,'biochemical exam','blood for s.bilirubin',150),(24,'biochemical exam','s.bilirubin direct/indirect',550),(25,'biochemical exam','blood for s.sgpt(alt)',350),(26,'biochemical exam','blood for sgot(ast)',350),(27,'biochemical exam','blood for alkaline pho:',400),(28,'biochemical exam','blood for urea',400),(29,'biochemical exam','blood for creatinine',400),(30,'biochemical exam','blood for bun test',350),(31,'biochemical exam','creatinine clearance',800),(32,'biochemical exam','lft (sgot ,sgpt,alkalinepho:)',1100),(33,'biochemical exam','blood for cholesterol',350),(34,'biochemical exam','triglyceride',300),(35,'biochemical exam','hdl cholesterol',300),(36,'biochemical exam','ldl cholesterol',300),(37,'biochemical exam','lipid profile',1000),(38,'biochemical exam','blood for calcium ',400),(39,'biochemical exam','blood for uric acid  ',400),(40,'biochemical exam','lft(sgor,sgpt,alkalinepho:)',1100),(41,'biochemical exam','blood for  s. cholestecol',350),(42,'biochemical exam','triglyccride',300),(43,'biochemical exam','hdl cholestecol',300),(44,'biochemical exam','lipis profile',1000),(45,'biochemical exam','blood for s. calcium',400),(46,'serologycal exam','widal test (6 titer)',350),(47,'serologycal exam','blood for hbs ag (screening)',300),(48,'serologycal exam','blood for vdrl',200),(49,'serologycal exam','blood for vdrl(q&q)test',700),(50,'serologycal exam','blood for tpha',400),(51,'serologycal exam','tpha (q&q) test',1300),(52,'serologycal exam','ecg',300),(53,'serologycal exam','echocardiography(black & white)',800),(54,'serologycal exam','echocardiography(color)',1500),(55,'serologycal exam','echocardiography(color doppler)',2000),(56,'serologycal exam','endoscopy of upper g.i.t',850),(57,'serologycal exam','video endoscopy of upper g.i.t',1500),(63,'biochemical exam','blood for s. amylase',1000),(64,'biochemical exam','urine for amylase',1000),(65,'biochemical exam','blood for s.ablumin',350),(66,'biochemical exam','blood for s.globubin',350),(67,'biochemical exam','total protein a/g ratio',700),(68,'biochemical exam','s. electrolytes',1000),(69,'biochemical exam','blood for aso titer',400),(70,'biochemical exam','blood for ra test',400),(71,'biochemical exam','rh.anti body titer',1000),(72,'biochemical exam','aldehyde test',300),(73,'biochemical exam','blood for c.r.p',850),(74,'biochemical exam','blood for c.r.p(elisa)',1100),(75,'biochemical exam','cft for filarial',850),(76,'biochemical exam','pregnancy test (hcg)test',150),(77,'biochemical exam','combs test/direct/indirect',1000),(78,'biochemical exam','blood grouping & rh factor',100),(79,'microbiology exam','tuberculin/mantoux(mt)',200),(80,'microbiology exam','sputum abf,r/e',150),(81,'microbiology exam','sputum for gram stain',150),(82,'microbiology exam','sputum abf c/s ',500),(83,'microbiology exam','p/s for gram stain',400),(84,'microbiology exam','p/s for c/s',500),(85,'microbiology exam','p/s collection charge',100),(86,'microbiology exam','h.v.s for gram stain',400),(87,'microbiology exam','h.v.s for c/s',500),(88,'microbiology exam','h.v.s collech charge',100),(89,'microbiology exam','t/s for klb',300),(90,'microbiology exam','t/s for r/e g/s',300),(91,'microbiology exam','t/s for c/s',500),(92,'microbiology exam','wound sawb /pus r/f',400),(93,'microbiology exam','wound sawb /pus c/s',500),(94,'microbiology exam','conjunctival swab for r/e',400),(95,'microbiology exam','conjunctival swab for c/s',500),(96,'microbiology exam','blood c/s',1000),(97,'microbiology exam','forgram stain',400),(98,'microbiology exam','skain scraping for fungus',350),(99,'microbiology exam','semen analysis',600),(100,'microbiology exam','body fluid:csf,pleural ,peritoneil ,sinovinl,ascitic(e)',400),(101,'urine exam','urine for r/e',100),(102,'urine exam','sp gravity',100),(103,'urine exam','urine for c/s',500),(104,'urine exam','24 urine total protain',700),(105,'stool exam','stool for r/f m/e',100),(106,'stool exam','stool obt',200),(107,'stool exam','stool r/s',100),(108,'stool exam','stool c/s',500),(109,'ultra sonography 4d color doppler exam','kub with prostate',1000),(110,'ultra sonography 4d color doppler exam','duplex study(any)',1300),(111,'ultra sonography 4d color doppler exam','thyroid gland 4d',1600),(114,'elisa test','t3,t4,tsh',2100),(115,'elisa test','hbs ag (confirmatory-flisa)',500),(116,'elisa test','ft3, ft4',2000),(117,'elisa test','testestcrone',1300),(118,'elisa test','prolactin ',1300),(119,'elisa test','h. pilori/anti.hev/fsh/lh/psa/hbe.ag/lge.(each)',1100),(120,'x-rey exam','x-ray chest p/a',250),(121,'x-rey exam','each iateral view',250),(122,'x-rey exam','each oblique view',250),(123,'x-rey exam','ba-meal s/d',600),(124,'x-rey exam','BA FOLLOW THROUGH',1200),(125,'x-rey exam','MANDIBLE B/A',500),(126,'x-rey exam','CREVICAL SPINR B/V',500),(127,'x-rey exam','KUB REGION (2 FILM)',500),(128,'x-rey exam','IVU (1300+100)',1400),(129,'x-rey exam','THORACIC SPINE B/V',500),(130,'x-rey exam','HIP JOINT B/V',500),(131,'x-rey exam','SHOULDER JOINT B/V',500),(132,'x-rey exam','CLAVICAL',500),(133,'x-rey exam','KNEE / ANKLE JOINT B/V',500),(134,'x-rey exam','BA FNAMA(1400+100)',1500),(135,'x-rey exam','PNS (OM)VIEW ',250),(136,'x-rey exam','PNS  both VIEW ',500),(137,'x-rey exam','skull b/v',500),(138,'x-rey exam','nasoparns l/a',250),(139,'x-rey exam','pelvis a/p view ',250),(140,'x-rey exam','abdomen a/p ',250),(141,'x-rey exam','dental x-ray (single)',120),(142,'x-rey exam','lumber spine b/v',500),(143,'x-rey exam','any joints bone',250),(144,'x-rey exam','scapular',500),(145,'x-rey exam','neck b/v',500),(146,'x-rey exam','o.c.g',900),(147,'x-rey exam','x-ray(1 film)',200),(148,'x-rey exam :(c.r machin)','each film',300),(149,'x-rey exam :(c.r machin)','opg',500),(150,'ultra sonography exam(b&w)','upper abdomen',750),(151,'ultra sonography exam(b&w)','hbs',750),(152,'ultra sonography exam(b&w)','lower abdomen',650),(153,'ultra sonography exam(b&w)','petal well being',650),(154,'ultra sonography exam(b&w)','kub prostate with pvr',850),(155,'ultra sonography exam(b&w)','breast(1)',700),(156,'ultra sonography exam(b&w)','breast(2)',1050),(157,'ultra sonography exam(b&w)','testis ',1050),(158,'ultra sonography exam(b&w)','thyroid gland',1050),(159,'ultra sonography exam(b&w)','transvaglnal(tvs)',1600),(160,'ultra sonography exam(b&w)','scrotum',1400),(161,'hormaones test','fsh ',1100),(162,'hormaones test','lh',1100),(163,'hormaones test','progestecronen',1200),(164,'hormaones test','estradiol/estrogen',1000),(165,'hormaones test','cortiso',1200),(166,'hormaones test','thyroglobuin(tg)',1200),(167,'hormaones test','anti thyroglobuin ab',1200),(168,'hormaones test','anti microsomal ab',1200),(169,'hormaones test','growth hormone',1200),(170,'hormaones test','para thyroid hormone',1200),(171,'hepatitis profile','hbe ag ',1100),(172,'hepatitis profile','anti -hbs',1200),(173,'hepatitis profile','anti-hbe',1200),(174,'hepatitis profile','anti-hcv',1100),(175,'hepatitis profile','anti-hbe lgm',1200),(176,'hepatitis profile','anti -hav lgm',1200),(177,'hepatitis profile','anti -hev lgm',1200),(178,'tumour markers','beta-hcg',1300),(179,'tumour markers','psa',1200),(180,'tumour markers','cea',1200),(181,'tumour markers','ca-19.9',1300),(182,'tumour markers','ca-125',1200),(183,'tumour markers','ca-15.3',1300),(184,'tumour markers','alpha feto protcin',1200),(185,'torch pannel','anti toxo plasma lgg',1200),(186,'torch pannel','anti toxo plasma lgm',1200),(187,'torch pannel','anti rubella lgg',1200),(188,'torch pannel','anti rubella lgm',1200),(189,'torch pannel','triple antigen',850),(190,'torch pannel','anti -hsv-1 lgg',1200),(191,'torch pannel','anti -hsv-1 lgm',1200),(192,'torch pannel','anti -hsv-2 lgg',1200),(193,'torch pannel','anti -hsv-2 lgm',1200),(194,'torch pannel','anti cmv-lgg',1200),(195,'torch pannel','anti cmv-lgm',1200),(196,'torch pannel','anti hiv (1&2)',1200),(197,'torch pannel','anti dengne lgg/lga',1200),(198,'serology','s.lgg /lgm/lge/lga',1200),(199,'serology','c3/c4',1200),(200,'serology','ana/anf',1200),(201,'serology','c-rp',850),(202,'serology','rose waaler',800),(203,'serology','cet for rala azer',1000),(204,'serology','cet for filarie',1000),(205,'serology','anti h pvlori lgg/lgm',1200),(206,'serology','febrile atigen',1200),(207,'serology','anti ds-dna',1300),(208,'serology','rh.antibody trict',1200),(209,'biochemical test','hba ic',1000),(210,'biochemical test','troponin-1',1400),(211,'biochemical test','electrophoresis',1200),(212,'biochemical test','s.forotin',1200),(213,'biochemical test','s.cpk',1000),(214,'biochemical test','s.ck-mb',1000),(215,'biochemical test','s.ldh',1000),(216,'biochemical test','s.albolase test',1000),(217,'biochemical test','s.iron',1000),(218,'biochemical test','s.tibc',1000),(219,'biochemical test','hby/dna',9000),(220,'biochemical test','s.i apose',1000),(221,'biochemical test','anti ccp',1800),(222,'biochemical test','condenic',1200),(223,'ultra sonography exam(b&w)','PRAGNACY PROFILE',1000),(224,'ultra sonography exam(b&w)','whole abdonen',850),(225,'ultra sonography exam(b&w)','FETKL WELL BRING',650),(226,'ultra sonography exam(b&w)','KUB WHIT PROSTATE',650),(227,'ultra sonography exam(b&w)','KUB  PROSTATE WHIT PVR',850),(228,'ultra sonography exam(b&w)','BREAST-1',700),(229,'ultra sonography exam(b&w)','BREAST-2',1050),(231,'ultra sonography 4d color doppler exam','Whole abdomen',1300),(232,'ultra sonography 4d color doppler exam','Whole abdomen & Pregnancy',1500),(233,'ultra sonography 4d color doppler exam','Fetal well Being (4D)',1300),(234,'ultra sonography 4d color doppler exam','Fetal well Being ',1000),(235,'ultra sonography 4d color doppler exam','Pevlvis Organ',1000),(236,'ultra sonography 4d color doppler exam','KuB  prostate With PVR',1300),(237,'ultra sonography 4d color doppler exam','Duplex Study (Any)',1600),(238,'ultra sonography 4d color doppler exam','Breast -1',1000),(239,'ultra sonography 4d color doppler exam','Breast -2',1500);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
