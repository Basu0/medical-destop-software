/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amin_ullah_medical;

import static amin_ullah_medical.Amin_seceond.p;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Basu dev
 */
public class Today extends javax.swing.JFrame {

    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    static int p=0;
    public Today() {
        initComponents();
        jTable1.getTableHeader().setFont(new Font("Tahamo",Font.BOLD,10));
        TableColumnModel colum=jTable1.getColumnModel();
        colum.getColumn(0).setPreferredWidth(30);
     Time();
        this.setResizable(false);
      //todayTabel();
        //colum.getColumn(1).setPreferredWidth(400);
        //colum.getColumn(2).setPreferredWidth(65);
    }
    
     public void con(){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/amin_ulla", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(Amin_seceond.class.getName()).log(Level.SEVERE, null, ex);
        }}

   public void todayTabel(){
        try { int a=0,b=0,c=0,d=0,e=0;
            con();
           // Date date = new Date();
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            String n = sdf.format(jd1.getDate());
            DefaultTableModel ta =(DefaultTableModel)jTable1.getModel();
            String m="Select `taka`,`DisCount`,`paid`,`unpaid` FROM `customar` WHERE `Date`='"+n+"'";
            st=con.prepareCall(m);
            ta.setRowCount(0);
            rs=st.executeQuery();
            while(rs.next()){
               a=ta.getRowCount();
               a=a+1;
               totalcs1.setText(""+a);
                b=b+rs.getInt(1);
               totalcs.setText(""+b);
               c=c+rs.getInt(2);
               discounds.setText(""+c);
               d=d+rs.getInt(3);
               paids.setText(""+d);
               e=e+rs.getInt(4);
               unpaids.setText(""+e);
                p++;
            ta.addRow(new Object[]{p,rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4)});
            }
        } catch (SQLException ex) {
            Logger.getLogger(Today.class.getName()).log(Level.SEVERE, null, ex);
        }
   
   }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        time_s = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        totalcs = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        discounds = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        paids = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        unpaids = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        totalcs1 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        age1 = new javax.swing.JLabel();
        jd1 = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setMinimumSize(new java.awt.Dimension(680, 670));
        jPanel5.setLayout(null);

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel6.setText("Date   :");
        jPanel5.add(jLabel6);
        jLabel6.setBounds(90, 100, 50, 30);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setText(".........................................................................................................................................................................");
        jPanel5.add(jLabel14);
        jLabel14.setBounds(90, 120, 410, 10);

        time_s.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jPanel5.add(time_s);
        time_s.setBounds(420, 100, 80, 30);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel3.setText("Time :");
        jPanel5.add(jLabel3);
        jLabel3.setBounds(370, 100, 50, 30);

        jLabel12.setFont(new java.awt.Font("Perpetua Titling MT", 1, 12)); // NOI18N
        jLabel12.setText("Day bill Receipt");
        jPanel5.add(jLabel12);
        jLabel12.setBounds(230, 80, 120, 20);

        jTable1.setFont(new java.awt.Font("Perpetua Titling MT", 0, 11)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "Total Taka", "Discount Taka", "Paid Taka", "Due Taka"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel5.add(jScrollPane1);
        jScrollPane1.setBounds(90, 140, 400, 390);

        jLabel2.setFont(new java.awt.Font("SutonnyMJ", 1, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(248, 107, 16));
        jLabel2.setText("Kzwgjøv evm ÷¨vÛ,‡dbx");
        jPanel5.add(jLabel2);
        jLabel2.setBounds(230, 40, 130, 20);

        jLabel11.setFont(new java.awt.Font("SutonnyMJ", 1, 14)); // NOI18N
        jLabel11.setText("‡gvevBj t 01829-561588, 01711-665627");
        jPanel5.add(jLabel11);
        jLabel11.setBounds(190, 50, 220, 30);

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/nn 75 pn2.png"))); // NOI18N
        jPanel5.add(jLabel13);
        jLabel13.setBounds(90, 0, 80, 80);

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel22.setText(".........................................................................................................................................................................");
        jPanel5.add(jLabel22);
        jLabel22.setBounds(90, 90, 410, 10);

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setText(".........................................................................................................................................................................");
        jPanel5.add(jLabel15);
        jLabel15.setBounds(90, 70, 410, 10);

        jLabel20.setFont(new java.awt.Font("Times New Roman", 1, 10)); // NOI18N
        jLabel20.setText("Software By: www.utcworld.net (01812-669757)");
        jPanel5.add(jLabel20);
        jLabel20.setBounds(290, 640, 210, 20);

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel16.setText("Signature");
        jPanel5.add(jLabel16);
        jLabel16.setBounds(90, 650, 80, 20);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("........................................");
        jPanel5.add(jLabel8);
        jLabel8.setBounds(90, 640, 100, 10);

        jLabel23.setBackground(new java.awt.Color(51, 102, 255));
        jLabel23.setFont(new java.awt.Font("SutonnyMJ", 1, 34)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(218, 87, 69));
        jLabel23.setText("Avwgb Djøvn& †gwW‡Kj †m›Uvi ");
        jPanel5.add(jLabel23);
        jLabel23.setBounds(170, 0, 340, 50);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setLayout(null);

        jLabel17.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel17.setText("Total Bill          :");
        jPanel1.add(jLabel17);
        jLabel17.setBounds(230, 0, 90, 20);

        totalcs.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jPanel1.add(totalcs);
        totalcs.setBounds(320, 0, 70, 20);

        jLabel18.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel18.setText("Discount           :");
        jPanel1.add(jLabel18);
        jLabel18.setBounds(230, 20, 90, 20);

        discounds.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jPanel1.add(discounds);
        discounds.setBounds(320, 20, 70, 20);

        jLabel19.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel19.setText("Paid amount      :");
        jPanel1.add(jLabel19);
        jLabel19.setBounds(230, 40, 90, 20);

        paids.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jPanel1.add(paids);
        paids.setBounds(320, 40, 70, 20);

        jLabel21.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel21.setText("Due Amount     :");
        jPanel1.add(jLabel21);
        jLabel21.setBounds(230, 60, 90, 20);

        unpaids.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jPanel1.add(unpaids);
        unpaids.setBounds(320, 60, 70, 20);

        jLabel24.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel24.setText("Total Test  :");
        jPanel1.add(jLabel24);
        jLabel24.setBounds(20, 30, 80, 20);

        totalcs1.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jPanel1.add(totalcs1);
        totalcs1.setBounds(100, 30, 70, 20);

        jPanel5.add(jPanel1);
        jPanel1.setBounds(90, 550, 400, 80);

        jLabel1.setFont(new java.awt.Font("Sylfaen", 3, 12)); // NOI18N
        jLabel1.setText("Note:");
        jPanel5.add(jLabel1);
        jLabel1.setBounds(90, 530, 32, 20);

        age1.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jPanel5.add(age1);
        age1.setBounds(160, 150, 20, 20);
        jPanel5.add(jd1);
        jd1.setBounds(140, 100, 140, 30);

        jButton2.setText("Show");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton2);
        jButton2.setBounds(279, 100, 90, 30);

        getContentPane().add(jPanel5);
        jPanel5.setBounds(0, 0, 520, 670);

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setText("Print");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(210, 670, 73, 30);

        setSize(new java.awt.Dimension(518, 747));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        PrinterJob job= PrinterJob.getPrinterJob();
        job.setJobName("Print Data");
        job.setPrintable(new Printable(){
        public int print(Graphics pg,PageFormat pf,int PageNum){
        if(PageNum>0){
        return Printable.NO_SUCH_PAGE;
        }
        Graphics2D g2=(Graphics2D) pg;
        g2.translate(pf.getImageableX(),pf.getImageableY());
        g2.scale(1,0.8);
        jPanel5.print(g2);
        return Printable.PAGE_EXISTS;
        }
        });
        boolean print=job.printDialog();
        if(print){
          JOptionPane.showMessageDialog(this,"Print Success");try{
        job.print();
        }
        catch(PrinterException ex){} }
        else{JOptionPane.showMessageDialog(this,"Printing Faild");}
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        p=0;
        todayTabel();
       // Time();
    }//GEN-LAST:event_jButton2ActionPerformed

    public void Time(){
    Date da=new Date();
    DateFormat timeFormat =new SimpleDateFormat("hh:mm:ss a");
    //String n = sdf.format(jd1.getDate());
    String time=timeFormat.format(da);
    time_s.setText(time);
    DateFormat dateformate =new SimpleDateFormat("dd-MM-yyyy");
    //String datef=dateformate.format(da);
  //  Date_s.setText(datef);
     }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Today.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Today.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Today.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Today.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Today().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel age1;
    private javax.swing.JLabel discounds;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable jTable1;
    private com.toedter.calendar.JDateChooser jd1;
    private javax.swing.JLabel paids;
    private javax.swing.JLabel time_s;
    private javax.swing.JLabel totalcs;
    private javax.swing.JLabel totalcs1;
    private javax.swing.JLabel unpaids;
    // End of variables declaration//GEN-END:variables
}
