/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amin_ullah_medical;

import static amin_ullah_medical.Insart_data.u;
import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Basu dev
 */
public class Account_creat extends javax.swing.JFrame {
Connection con=null;
    PreparedStatement st=null;
    ResultSet rs=null;
    public Account_creat() {
        initComponents();
        tabelShow();
        this.setResizable(false);
        jTable1.getTableHeader().setFont(new Font("Tahamo",Font.BOLD,14));
    }

  public void con(){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/amin_ulla", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(Account_creat.class.getName()).log(Level.SEVERE, null, ex);
        }}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Userin = new javax.swing.JTextField();
        passwordin = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        Si_no = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(0, 0, 128));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Password :");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(290, 60, 80, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("User Name :");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(30, 60, 90, 30);

        Userin.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jPanel1.add(Userin);
        Userin.setBounds(130, 60, 150, 30);

        passwordin.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jPanel1.add(passwordin);
        passwordin.setBounds(380, 60, 170, 30);

        jPanel2.setBackground(new java.awt.Color(0, 0, 128));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));

        jLabel3.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Create A New Account");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(179, 179, 179)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(215, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 620, 50);

        jTable1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Serial No", "User Name", "Password"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(0, 100, 620, 200);

        jLabel4.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Serial No:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(4, 320, 66, 30);

        Si_no.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jPanel1.add(Si_no);
        Si_no.setBounds(80, 320, 70, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("User Name :");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(154, 320, 90, 30);

        jTextField4.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jPanel1.add(jTextField4);
        jTextField4.setBounds(250, 320, 130, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Password :");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(380, 320, 80, 30);

        jTextField5.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jPanel1.add(jTextField5);
        jTextField5.setBounds(470, 320, 130, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Cancel_35px_1.png"))); // NOI18N
        jLabel7.setText("Cancel");
        jLabel7.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel7MouseMoved(evt);
            }
        });
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel7MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel7MouseExited(evt);
            }
        });
        jPanel1.add(jLabel7);
        jLabel7.setBounds(470, 370, 110, 30);

        jLabel8.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Add_User_Group_Man_Man_35px.png"))); // NOI18N
        jLabel8.setText("Submit ");
        jLabel8.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel8MouseMoved(evt);
            }
        });
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel8MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel8MouseExited(evt);
            }
        });
        jPanel1.add(jLabel8);
        jLabel8.setBounds(20, 370, 120, 30);

        jLabel9.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Trash_Can_35px.png"))); // NOI18N
        jLabel9.setText("Delete ");
        jLabel9.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel9MouseMoved(evt);
            }
        });
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel9MouseExited(evt);
            }
        });
        jPanel1.add(jLabel9);
        jLabel9.setBounds(160, 370, 120, 30);

        jLabel10.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Change_User_35px.png"))); // NOI18N
        jLabel10.setText("Update ");
        jLabel10.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel10MouseMoved(evt);
            }
        });
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel10MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel10MouseExited(evt);
            }
        });
        jPanel1.add(jLabel10);
        jLabel10.setBounds(310, 370, 120, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 630, 440);

        setSize(new java.awt.Dimension(633, 467));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseClicked
        // TODO add your handling code here:
         addAccount();
         DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
       while(dm.getRowCount()>0){
        for (int i = 0; i <dm.getRowCount(); i++) {
            dm.removeRow(i);
        }}
       tabelShow();
    }//GEN-LAST:event_jLabel8MouseClicked

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        DefaultTableModel ta=(DefaultTableModel)jTable1.getModel();
        Si_no.setText(ta.getValueAt(jTable1.getSelectedRow(), 0).toString());
        jTextField4.setText(ta.getValueAt(jTable1.getSelectedRow(), 1).toString());
        jTextField5.setText(ta.getValueAt(jTable1.getSelectedRow(), 2).toString());
    }//GEN-LAST:event_jTable1MouseClicked

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"You Sure Delete Test","Test", JOptionPane.WARNING_MESSAGE);
        if(result==0){
        if(jTextField4.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "You Select Test");
       }else{
        
            con();
           String x=jTextField4.getText();
           String u="DELETE FROM `sequrity` WHERE `stap_User_Name` ='"+x+"'";
            try {
                st=con.prepareCall(u);
            } catch (SQLException ex) {
                Logger.getLogger(Account_creat.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(Account_creat.class.getName()).log(Level.SEVERE, null, ex);
            }
          DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
       while(dm.getRowCount()>0){
        for (int i = 0; i <dm.getRowCount(); i++) {
            dm.removeRow(i);
        }}}}
       tabelShow(); 
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jLabel10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseClicked
    try {
        // TODO add your handling code here:
        con();
        int x=Integer.parseInt(Si_no.getText().toString());
        String u="UPDATE `sequrity` SET `stap_User_Name`=?,`stap_Password`=? WHERE `Id` ='"+x+"'";
        st=con.prepareCall(u);
        st.setString(1,jTextField4.getText());
        st.setString(2,jTextField5.getText());
        st.executeUpdate();
        DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
        while(dm.getRowCount()>0){
            for (int i = 0; i <dm.getRowCount(); i++) {
                dm.removeRow(i);
            }}
        tabelShow(); 
    } catch (SQLException ex) {
        Logger.getLogger(Account_creat.class.getName()).log(Level.SEVERE, null, ex);
    }
    }//GEN-LAST:event_jLabel10MouseClicked

    private void jLabel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseClicked
        // TODO add your handling code here:
        Userin.setText("");
        passwordin.setText("");
        Si_no.setText("");
        jTextField4.setText("");
        jTextField5.setText("");
        
    }//GEN-LAST:event_jLabel7MouseClicked

    private void jLabel8MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseMoved
        // TODO add your handling code here:
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel8MouseMoved

    private void jLabel8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseExited
        // TODO add your handling code here:
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0,0,128)));
    }//GEN-LAST:event_jLabel8MouseExited

    private void jLabel9MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseMoved
        // TODO add your handling code here:
        jLabel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel9MouseMoved

    private void jLabel9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseExited
        // TODO add your handling code here:
         jLabel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0,0,128)));
    }//GEN-LAST:event_jLabel9MouseExited

    private void jLabel10MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseMoved
        // TODO add your handling code here:
          jLabel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel10MouseMoved

    private void jLabel10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseExited
        // TODO add your handling code here:
        jLabel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0,0,128)));
    }//GEN-LAST:event_jLabel10MouseExited

    private void jLabel7MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseMoved
        // TODO add your handling code here:
         jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel7MouseMoved

    private void jLabel7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseExited
        // TODO add your handling code here:
        jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0,0,128)));
    }//GEN-LAST:event_jLabel7MouseExited

    public void addAccount(){
    try {
        con();
        String q="INSERT INTO `sequrity`(`stap_User_Name`,`stap_Password`) VALUES (?,?)";
        st=con.prepareCall(q);
        st.setString(1,Userin.getText());
         st.setString(2,passwordin.getText());
         st.executeUpdate();
    } catch (SQLException ex) {
        Logger.getLogger(Account_creat.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    public void tabelShow(){
    try {
        con();
        DefaultTableModel ta =(DefaultTableModel)jTable1.getModel();
        String R="SELECT `Id`,`stap_User_Name`,`stap_Password` FROM `sequrity`";
        st=con.prepareCall(R);
        rs=st.executeQuery();
        while(rs.next()){
          ta.addRow(new Object[]{rs.getString(1),rs.getString(2),rs.getString(3)});  
        }
    } catch (SQLException ex) {
        Logger.getLogger(Account_creat.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Account_creat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Account_creat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Account_creat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Account_creat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Account_creat().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Si_no;
    private javax.swing.JTextField Userin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField passwordin;
    // End of variables declaration//GEN-END:variables
}
