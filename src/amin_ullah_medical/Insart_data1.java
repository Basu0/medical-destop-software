/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amin_ullah_medical;

import com.sun.glass.events.KeyEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Basu dev
 */
public class Insart_data1 extends javax.swing.JFrame {
Connection con=null;
    PreparedStatement st=null;
    ResultSet rs=null;
    static String u=null;
    static String p=null;
    int d=0;
    public Insart_data1() {
        //this.setAlwaysOnTop(true);
        //this.setResizable(false);
        //this.setVisible(true);
        initComponents();
        Toolkit t=Toolkit.getDefaultToolkit();
        int x=(int) t.getScreenSize().getHeight();
        int y=(int) t.getScreenSize().getWidth();
        this.setSize(y,x);
        SI_NO.setText(""+ tableShow());
        jTable1.getTableHeader().setFont(new Font("Tahamo",Font.BOLD,14));
        catagory();
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        TestCatagory_combo = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel14 = new javax.swing.JLabel();
        SI_NO = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(0, 0, 128));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        jPanel1.setLayout(null);

        jLabel5.setBackground(new java.awt.Color(0, 0, 128));
        jLabel5.setFont(new java.awt.Font("Sitka Heading", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Clear_Symbol_35px.png"))); // NOI18N
        jLabel5.setText("   CLEAR");
        jLabel5.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel5MouseMoved(evt);
            }
        });
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel5MouseExited(evt);
            }
        });
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 400, 220, 40);

        jLabel6.setBackground(new java.awt.Color(0, 0, 128));
        jLabel6.setFont(new java.awt.Font("Sitka Heading", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Plus_Math_35px.png"))); // NOI18N
        jLabel6.setText("   ADD TEST");
        jLabel6.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel6MouseMoved(evt);
            }
        });
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel6MouseExited(evt);
            }
        });
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 150, 220, 40);

        jLabel7.setBackground(new java.awt.Color(0, 0, 128));
        jLabel7.setFont(new java.awt.Font("Sitka Heading", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Update_35px.png"))); // NOI18N
        jLabel7.setText("   UPDATE");
        jLabel7.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel7MouseMoved(evt);
            }
        });
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel7MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel7MouseExited(evt);
            }
        });
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 220, 220, 40);

        jLabel8.setBackground(new java.awt.Color(0, 0, 128));
        jLabel8.setFont(new java.awt.Font("Sitka Heading", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Delete_Database_35px.png"))); // NOI18N
        jLabel8.setText("   DELETE");
        jLabel8.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel8MouseMoved(evt);
            }
        });
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel8MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel8MouseExited(evt);
            }
        });
        jPanel1.add(jLabel8);
        jLabel8.setBounds(10, 280, 220, 40);

        jLabel9.setBackground(new java.awt.Color(0, 0, 128));
        jLabel9.setFont(new java.awt.Font("Sitka Heading", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Cancel_35px.png"))); // NOI18N
        jLabel9.setText("   CANCEL");
        jLabel9.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel9MouseMoved(evt);
            }
        });
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel9MouseExited(evt);
            }
        });
        jPanel1.add(jLabel9);
        jLabel9.setBounds(10, 520, 220, 40);

        jLabel10.setFont(new java.awt.Font("SansSerif", 1, 22)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Amin Ullah Medical ");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(10, 60, 210, 40);

        jLabel11.setFont(new java.awt.Font("SansSerif", 1, 22)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Centre");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(70, 100, 70, 30);

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/nn 75 pn.png"))); // NOI18N
        jPanel1.add(jLabel12);
        jLabel12.setBounds(70, 0, 80, 70);

        jSeparator1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jSeparator1);
        jSeparator1.setBounds(0, 138, 240, 2);

        jLabel18.setBackground(new java.awt.Color(0, 0, 128));
        jLabel18.setFont(new java.awt.Font("Sitka Heading", 1, 18)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Edit_File_35px.png"))); // NOI18N
        jLabel18.setText("   CATAGORY EDIT");
        jLabel18.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel18MouseMoved(evt);
            }
        });
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel18MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel18MouseExited(evt);
            }
        });
        jPanel1.add(jLabel18);
        jLabel18.setBounds(10, 340, 220, 40);

        jLabel19.setBackground(new java.awt.Color(0, 0, 128));
        jLabel19.setFont(new java.awt.Font("Sitka Heading", 1, 18)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Refresh_35px.png"))); // NOI18N
        jLabel19.setText("   Refresh");
        jLabel19.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jLabel19AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jLabel19.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel19MouseMoved(evt);
            }
        });
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel19MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel19MouseExited(evt);
            }
        });
        jPanel1.add(jLabel19);
        jLabel19.setBounds(10, 460, 220, 40);
        jPanel1.add(jSeparator2);
        jSeparator2.setBounds(0, 510, 240, 10);
        jPanel1.add(jSeparator3);
        jSeparator3.setBounds(0, 210, 240, 20);
        jPanel1.add(jSeparator4);
        jSeparator4.setBounds(0, 270, 240, 20);
        jPanel1.add(jSeparator5);
        jSeparator5.setBounds(0, 330, 240, 20);
        jPanel1.add(jSeparator6);
        jSeparator6.setBounds(0, 390, 240, 20);
        jPanel1.add(jSeparator7);
        jSeparator7.setBounds(0, 450, 240, 10);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 240, 770);

        jPanel2.setBackground(new java.awt.Color(0, 0, 128));
        jPanel2.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("NEW CATEGORY :");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(140, 30, 160, 30);

        jTextField1.setBackground(new java.awt.Color(95, 75, 139));
        jTextField1.setFont(new java.awt.Font("Perpetua Titling MT", 1, 15)); // NOI18N
        jTextField1.setForeground(new java.awt.Color(255, 255, 255));
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });
        jPanel2.add(jTextField1);
        jTextField1.setBounds(140, 70, 220, 40);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("CATEGORY :");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(450, 30, 120, 30);

        TestCatagory_combo.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        TestCatagory_combo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Test Category" }));
        TestCatagory_combo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TestCatagory_comboKeyPressed(evt);
            }
        });
        jPanel2.add(TestCatagory_combo);
        TestCatagory_combo.setBounds(450, 70, 260, 40);

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("TEST NAME :");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(730, 30, 120, 30);

        jTextField2.setBackground(new java.awt.Color(95, 75, 139));
        jTextField2.setFont(new java.awt.Font("Perpetua Titling MT", 1, 18)); // NOI18N
        jTextField2.setForeground(new java.awt.Color(255, 255, 255));
        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField2KeyPressed(evt);
            }
        });
        jPanel2.add(jTextField2);
        jTextField2.setBounds(730, 70, 250, 40);

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("TAKA :");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(1000, 30, 70, 30);

        jTextField3.setBackground(new java.awt.Color(95, 75, 139));
        jTextField3.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jTextField3.setForeground(new java.awt.Color(255, 255, 255));
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField3KeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField3KeyTyped(evt);
            }
        });
        jPanel2.add(jTextField3);
        jTextField3.setBounds(1000, 70, 100, 40);

        jCheckBox1.setBackground(new java.awt.Color(51, 255, 51));
        jCheckBox1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jCheckBox1.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox1.setText("Add ");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });
        jPanel2.add(jCheckBox1);
        jCheckBox1.setBounds(360, 70, 60, 40);

        jLabel14.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("SI NO :");
        jPanel2.add(jLabel14);
        jLabel14.setBounds(20, 30, 60, 30);

        SI_NO.setBackground(new java.awt.Color(95, 75, 139));
        SI_NO.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        SI_NO.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(SI_NO);
        SI_NO.setBounds(20, 70, 100, 40);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(240, 60, 1200, 140);

        jTable1.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        jTable1.setForeground(new java.awt.Color(0, 0, 128));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NO", "TEST NAME", "TAKA", "CATEGORY"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setName(""); // NOI18N
        jTable1.setRowHeight(20);
        jTable1.setRowMargin(5);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(240, 270, 1170, 510);

        jPanel4.setBackground(new java.awt.Color(138, 43, 226));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        jPanel4.setLayout(null);

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Category_40px.png"))); // NOI18N
        jLabel13.setText("Total Test Category   :");
        jPanel4.add(jLabel13);
        jLabel13.setBounds(630, 10, 220, 50);

        jLabel15.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jPanel4.add(jLabel15);
        jLabel15.setBounds(870, 10, 90, 50);

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Database_40px.png"))); // NOI18N
        jLabel16.setText("Total Test Add :");
        jPanel4.add(jLabel16);
        jLabel16.setBounds(260, 10, 180, 50);

        jLabel17.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jPanel4.add(jLabel17);
        jLabel17.setBounds(460, 10, 90, 50);

        getContentPane().add(jPanel4);
        jPanel4.setBounds(240, 200, 1200, 70);

        jPanel3.setBackground(new java.awt.Color(68, 10, 127));
        jPanel3.setLayout(null);

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Minimize_Window_35px.png"))); // NOI18N
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel20);
        jLabel20.setBounds(1020, 10, 40, 40);

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Close_Window_35px_1.png"))); // NOI18N
        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel21MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel21);
        jLabel21.setBounds(1070, 10, 35, 40);

        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/nn 75 pn.png"))); // NOI18N
        jPanel3.add(jLabel24);
        jLabel24.setBounds(180, 0, 80, 60);

        jLabel22.setBackground(new java.awt.Color(51, 102, 255));
        jLabel22.setFont(new java.awt.Font("SutonnyMJ", 1, 50)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(218, 87, 69));
        jLabel22.setText("Avwgb Djøvn& †gwW‡Kj †m›Uvi ");
        jPanel3.add(jLabel22);
        jLabel22.setBounds(330, 0, 500, 60);

        getContentPane().add(jPanel3);
        jPanel3.setBounds(240, 0, 1200, 60);

        setBounds(0, 0, 1406, 763);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
         jTextField1.setText("");
         jTextField2.setText("");
         jTextField3.setText("");
         TestCatagory_combo.setSelectedIndex(0);
         SI_NO.setText("");
    }//GEN-LAST:event_jLabel5MouseClicked

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
    try {
        // TODO add your handling code here:
        con();
        u=jTextField2.getText();
        String R="SELECT `Test_name`FROM `test` WHERE `Test_name`='"+u+"'";
        st=con.prepareCall(R);
        rs=st.executeQuery();
        if(rs.next()){
            JOptionPane.showMessageDialog(this,"Already Test Add");
        }else{
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"You Sure Add Data","Test", JOptionPane.INFORMATION_MESSAGE);
        if(result==0){
            
            if(jTextField2.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(this, "Test Name IS Empty");
                jTextField2.requestFocus();
            }
            else if(jTextField3.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(this, "Taka  is Empty");
                jTextField3.requestFocus();
            }
            else{
                try {
                    con();
                    st=null;
                    String q="INSERT INTO `test`(`Test_catagory`,`Test_name`,`Test_Price`) VALUES (?,?,?)";
                    st=con.prepareCall(q);
                    st.setString(1,TestCatagory_combo.getSelectedItem().toString());
                    st.setString(2,jTextField2.getText());
                    st.setString(3,jTextField3.getText());
                    if(TestCatagory_combo.getSelectedIndex()==0){
                    JOptionPane.showMessageDialog(this,"Select Test Category");
                    TestCatagory_combo.requestFocus();
                    }else{
                    st.executeUpdate();
                    JOptionPane.showMessageDialog(this,"SuccessFull...");
                    DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
                    while(dm.getRowCount()>0){
                        for (int j = 0; j <dm.getRowCount(); j++) {
                            dm.removeRow(j);
                        }}
                    tableShow();}
                } catch (SQLException ex) {
                    Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
                }}}}
    } catch (SQLException ex) {
        Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
    } 
    }//GEN-LAST:event_jLabel6MouseClicked

    private void jLabel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseClicked
        // TODO add your handling code here:
        if(jTextField2.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "Test Name IS Empty");
            jTextField2.requestFocus();
       }
       else if(jTextField3.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "Taka  is Empty");
            jTextField3.requestFocus();
       }
       else{
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"You Sure Update ");
        if(result==0)
        {
        try {
            con();
            int x=Integer.parseInt(SI_NO.getText().toString());
           String u="UPDATE `test` SET `Test_name`=?,`Test_Price`=? WHERE `SID` ='"+x+"'";
            st=con.prepareCall(u);
            st.setString(1,jTextField2.getText());
            st.setString(2,jTextField3.getText());
            st.executeUpdate();
          DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
       while(dm.getRowCount()>0){
        for (int i = 0; i <dm.getRowCount(); i++) {
            dm.removeRow(i);
        }}
         tableShow();
        } catch (SQLException ex) {
            Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
        }}}
    }//GEN-LAST:event_jLabel7MouseClicked

    private void jLabel8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseClicked
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"You Sure Delete Test","Test", JOptionPane.WARNING_MESSAGE);
        if(result==0){
        if(jTextField2.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "You Select Test");
       }else{
        try {
            con();
            int x=Integer.parseInt(SI_NO.getText().toString());
           String u="DELETE FROM `test` WHERE `SID` ='"+x+"'";
            st=con.prepareCall(u);
            st.executeUpdate();
          DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
       while(dm.getRowCount()>0){
        for (int i = 0; i <dm.getRowCount(); i++) {
            dm.removeRow(i);
        }}
         tableShow();
         jTextField1.setText("");
         jTextField2.setText("");
         jTextField3.setText("");
         TestCatagory_combo.setSelectedIndex(0);
         
        } catch (SQLException ex) {
            Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
        }}}
    }//GEN-LAST:event_jLabel8MouseClicked

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"Exit Apps ");
        if(result==0){
            System.exit(0);
        }
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
       
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"You Sure Add Data","Test", JOptionPane.INFORMATION_MESSAGE);
        if(result==0){
        try { if(jTextField1.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "Entry Catagory Name !");
       }
    else{  TestCatagory_combo.setSelectedIndex(0);       
        con();
   u=jTextField1.getText();
  String Q="SELECT `Test_catagory`,`ID` FROM `catagory` WHERE `Test_catagory`='"+u+"'";
        
            st=con.prepareCall(Q);
            rs=st.executeQuery();
            if(rs.next()){
           JOptionPane.showMessageDialog(this,"Already Data Add");
            }
            else{
       newCatagoriIn();
       catagory();
            }}}
            
        catch (SQLException ex) {
            Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
        }}
        
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        DefaultTableModel ta=(DefaultTableModel)jTable1.getModel();
        jTextField1.setText(ta.getValueAt(jTable1.getSelectedRow(), 3).toString());
        jTextField2.setText(ta.getValueAt(jTable1.getSelectedRow(), 1).toString());
        jTextField3.setText(ta.getValueAt(jTable1.getSelectedRow(), 2).toString());
        SI_NO.setText(ta.getValueAt(jTable1.getSelectedRow(), 0).toString());
        TestCatagory_combo.setSelectedItem(ta.getValueAt(jTable1.getSelectedRow(), 3).toString());
    }//GEN-LAST:event_jTable1MouseClicked

    private void jLabel6MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseMoved
        // TODO add your handling code here:
        jLabel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel6MouseMoved

    private void jLabel6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseExited
        // TODO add your handling code here:
        jLabel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    }//GEN-LAST:event_jLabel6MouseExited

    private void jLabel7MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseMoved
        // TODO add your handling code here:
        jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel7MouseMoved

    private void jLabel7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseExited
        // TODO add your handling code here:
        jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    }//GEN-LAST:event_jLabel7MouseExited

    private void jLabel8MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseMoved
        // TODO add your handling code here:
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel8MouseMoved

    private void jLabel8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseExited
        // TODO add your handling code here:
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    }//GEN-LAST:event_jLabel8MouseExited

    private void jLabel5MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseMoved
        // TODO add your handling code here:
        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel5MouseMoved

    private void jLabel5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseExited
        // TODO add your handling code here:
        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    }//GEN-LAST:event_jLabel5MouseExited

    private void jLabel9MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseMoved
        // TODO add your handling code here:
        jLabel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel9MouseMoved

    private void jLabel9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseExited
        // TODO add your handling code here:
        jLabel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    }//GEN-LAST:event_jLabel9MouseExited

    private void jTextField3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
         if(!(Character.isDigit(c) || c==KeyEvent.VK_SPACE || c==KeyEvent.VK_DELETE)){
         evt.consume();
         }
    }//GEN-LAST:event_jTextField3KeyTyped

    private void jLabel18MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseMoved
        // TODO add your handling code here:
        jLabel18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel18MouseMoved

    private void jLabel18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseClicked
        // TODO add your handling code here:
        CatagoryEdit CA=new CatagoryEdit();
        CA.setVisible(true);
    }//GEN-LAST:event_jLabel18MouseClicked

    private void jLabel18MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseExited
        // TODO add your handling code here:
        jLabel18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    
    }//GEN-LAST:event_jLabel18MouseExited

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
          
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
              int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"You Sure Add Data","Test", JOptionPane.INFORMATION_MESSAGE);
        if(result==0){ 
        try { 
            if(jTextField1.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "Entry Catagory Name !");
       }else{        
        con();
   u=jTextField1.getText();
  String Q="SELECT `Test_catagory`,`ID` FROM `catagory` WHERE `Test_catagory`='"+u+"'";
        
            st=con.prepareCall(Q);
            rs=st.executeQuery();
            if(rs.next()){
           JOptionPane.showMessageDialog(this,"Already Data Add");
            }
            else{
       newCatagoriIn();
       catagory();}}}
            
         catch (SQLException ex) {
            Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
        }
        }}
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jLabel6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel6MouseEntered

    private void TestCatagory_comboKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TestCatagory_comboKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        jTextField2.requestFocus();
        }
    }//GEN-LAST:event_TestCatagory_comboKeyPressed

    private void jTextField2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            jTextField3.setText("");
        jTextField3.requestFocus();
        }
    }//GEN-LAST:event_jTextField2KeyPressed

    private void jLabel19MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseMoved
        // TODO add your handling code here:
      jLabel19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));

    }//GEN-LAST:event_jLabel19MouseMoved

    private void jLabel19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseClicked
        // TODO add your handling code here:
        Insart_data1 re=new Insart_data1();
        re.setVisible(true);
        this.dispose();
        
    }//GEN-LAST:event_jLabel19MouseClicked

    private void jLabel19MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseExited
        // TODO add your handling code here:
   jLabel19.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));

    }//GEN-LAST:event_jLabel19MouseExited

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
        // TODO add your handling code here:
        this.setState(JFrame.ICONIFIED);
    }//GEN-LAST:event_jLabel20MouseClicked

    private void jLabel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseClicked
        // TODO add your handling code here:
         int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"Exit Apps ");
        if(result==0){
        dispose();
        }
    }//GEN-LAST:event_jLabel21MouseClicked

    private void jLabel19AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jLabel19AncestorAdded
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jLabel19AncestorAdded

    private void jTextField3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyPressed
        // TODO add your handling code here:
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        try {
        // TODO add your handling code here:
        con();
        u=jTextField2.getText();
        String R="SELECT `Test_name`FROM `test` WHERE `Test_name`='"+u+"'";
        st=con.prepareCall(R);
        rs=st.executeQuery();
        if(rs.next()){
            JOptionPane.showMessageDialog(this,"Already Test Add");
        }else{
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"You Sure Add Data","Test", JOptionPane.INFORMATION_MESSAGE);
        if(result==0){
            
            if(jTextField2.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(this, "Test Name IS Empty");
                jTextField2.requestFocus();
            }
            else if(jTextField3.getText().isEmpty())
            {
                JOptionPane.showMessageDialog(this, "Taka  is Empty");
                jTextField3.requestFocus();
            }
            else{
                try {
                    con();
                    st=null;
                    String q="INSERT INTO `test`(`Test_catagory`,`Test_name`,`Test_Price`) VALUES (?,?,?)";
                    st=con.prepareCall(q);
                    st.setString(1,TestCatagory_combo.getSelectedItem().toString());
                    st.setString(2,jTextField2.getText());
                    st.setString(3,jTextField3.getText());
                    st.executeUpdate();
                    JOptionPane.showMessageDialog(this,"Sucess Full");
                    DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
                    while(dm.getRowCount()>0){
                        for (int j = 0; j <dm.getRowCount(); j++) {
                            dm.removeRow(j);
                        }}
                    tableShow();
                    jTextField3.setText("");
                    jTextField2.setText("");
                    jTextField2.requestFocus();
                } catch (SQLException ex) {
                    Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
                }}}}
    } catch (SQLException ex) {
        Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
    } 
        }
    }//GEN-LAST:event_jTextField3KeyPressed

   public void con(){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/amin_ulla", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
        }}
   public void catagory(){
    con();
    TestCatagory_combo.setSelectedIndex(0);
  //TestCatagory_combo.addItem("Select Test Category");
    String Q="SELECT `Test_catagory` FROM `catagory`";
        try {
            st=con.prepareCall(Q);
            rs=st.executeQuery();
            while(rs.next()){
            TestCatagory_combo.addItem(rs.getString(1));
            }
         int cn=TestCatagory_combo.getItemCount();
        cn=cn-1;
        jLabel15.setText(""+cn);
        } catch (SQLException ex) {
            Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   public int tableShow(){
    con();
   st=null;
   int p=0;
    DefaultTableModel ta =(DefaultTableModel)jTable1.getModel();
   String Q="SELECT `SID`,`Test_name`,`Test_Price`,`Test_catagory` FROM`test`";
        try {
            st=con.prepareCall(Q);
            rs=st.executeQuery();
            while(rs.next())
            { ta.addRow(new Object[]{rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4)});
            d=rs.getInt(1);}
            d=d+1;
            p=ta.getRowCount();
            p=p+1;
            jLabel17.setText(""+p);
          } catch (SQLException ex) {
            Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
        }
   return d;}
   
public void newCatagoriIn(){
   try {
        
        TestCatagory_combo.setSelectedIndex(0);
            con();
        st=null;
        String q="INSERT INTO `catagory`(`Test_catagory`) VALUES (?)";
        st=con.prepareCall(q);
         st.setString(1,jTextField1.getText());
         st.executeUpdate();
         JOptionPane.showMessageDialog(this,"Sucess Full");
    } catch (SQLException ex) {
        Logger.getLogger(Insart_data1.class.getName()).log(Level.SEVERE, null, ex);
    }
}

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Insart_data1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Insart_data1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Insart_data1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Insart_data1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Insart_data1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField SI_NO;
    private javax.swing.JComboBox<String> TestCatagory_combo;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    // End of variables declaration//GEN-END:variables
}
