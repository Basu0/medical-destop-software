/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amin_ullah_medical;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Basu dev
 */
public class Customar_PROFILE extends javax.swing.JFrame {
   Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    public Customar_PROFILE() {
        initComponents();
        this.setResizable(false);
    }

    public void con(){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/amin_ulla", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(Amin_seceond.class.getName()).log(Level.SEVERE, null, ex);
        }}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        ID = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        Name = new javax.swing.JLabel();
        Age = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        Search = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        gendar = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        Mobile = new javax.swing.JLabel();
        Address = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        time = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        TotalTk = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        Discount = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        paid = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        duetk = new javax.swing.JLabel();
        D_Name = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        test = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(0, 0, 128));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Customar profile");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(190, 190, 190)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(226, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 600, 60);

        jPanel2.setBackground(new java.awt.Color(75, 114, 99));
        jPanel2.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Serial No  :");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(30, 50, 70, 31);

        ID.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ID.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(ID);
        ID.setBounds(110, 50, 200, 31);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Search_30px.png"))); // NOI18N
        jPanel2.add(jLabel2);
        jLabel2.setBounds(160, 0, 30, 40);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Name      :");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(30, 80, 70, 31);

        Name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Name.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(Name);
        Name.setBounds(110, 80, 200, 31);

        Age.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Age.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(Age);
        Age.setBounds(110, 120, 200, 31);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Age         :");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(30, 120, 70, 31);

        Search.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Search.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SearchKeyPressed(evt);
            }
        });
        jPanel2.add(Search);
        Search.setBounds(160, 0, 250, 40);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Gendar   :");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(30, 160, 70, 31);

        gendar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        gendar.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(gendar);
        gendar.setBounds(110, 160, 200, 31);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Mobile    :");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(30, 200, 70, 31);

        Mobile.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Mobile.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(Mobile);
        Mobile.setBounds(110, 200, 200, 31);

        Address.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Address.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(Address);
        Address.setBounds(110, 240, 200, 31);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Address :");
        jPanel2.add(jLabel12);
        jLabel12.setBounds(30, 240, 70, 31);

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Date  :");
        jPanel2.add(jLabel13);
        jLabel13.setBounds(330, 50, 60, 31);

        date.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        date.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(date);
        date.setBounds(420, 50, 150, 31);

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Time  :");
        jPanel2.add(jLabel15);
        jLabel15.setBounds(330, 90, 60, 31);

        time.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        time.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(time);
        time.setBounds(420, 90, 150, 31);

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Total Tk :");
        jPanel2.add(jLabel17);
        jLabel17.setBounds(330, 130, 60, 31);

        TotalTk.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TotalTk.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(TotalTk);
        TotalTk.setBounds(420, 130, 150, 31);

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Discount Tk :");
        jPanel2.add(jLabel19);
        jLabel19.setBounds(330, 170, 90, 31);

        Discount.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Discount.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(Discount);
        Discount.setBounds(420, 170, 150, 31);

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Paid Tk :");
        jPanel2.add(jLabel21);
        jLabel21.setBounds(330, 210, 60, 31);

        paid.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        paid.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(paid);
        paid.setBounds(420, 210, 150, 31);

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Due Tk  :");
        jPanel2.add(jLabel23);
        jLabel23.setBounds(330, 250, 60, 31);

        duetk.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        duetk.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(duetk);
        duetk.setBounds(420, 250, 150, 31);

        D_Name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        D_Name.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(D_Name);
        D_Name.setBounds(130, 300, 450, 31);

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setText("Doctor Name :");
        jPanel2.add(jLabel26);
        jLabel26.setBounds(30, 300, 100, 31);

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setText("Test              :");
        jPanel2.add(jLabel28);
        jLabel28.setBounds(30, 340, 100, 50);

        test.setColumns(20);
        test.setFont(new java.awt.Font("Monospaced", 1, 13)); // NOI18N
        test.setRows(5);
        jScrollPane1.setViewportView(test);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(126, 336, 460, 60);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Search :");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(100, 0, 60, 40);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 60, 600, 420);

        setSize(new java.awt.Dimension(611, 503));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void SearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SearchKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==com.sun.glass.events.KeyEvent.VK_ENTER){
        Show();
        }
    }//GEN-LAST:event_SearchKeyPressed

    private void rSButtonRiple1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSButtonRiple1ActionPerformed
        // TODO add your handling code here:
        if(Search.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "Entry Serial No or Number or Name !");
            Search.requestFocus();
       }else{
       con();
       st=null;
       try {
       String x=Search.getText();
       String m="Select `Id`,`Customar_Name`,`Age`,`Gendar`,`Mobile`,`Address`,`Dr_Name`,`Date`,`Time`,`taka`,`DisCount`,`paid`,`unpaid`,`Tests`FROM `customar` WHERE `Id`='"+x+"'or`Customar_Name`='"+x+"'or`Mobile`='"+x+"'";
       st=con.prepareCall(m);
       rs=st.executeQuery();
       while(rs.next()){
        ID.setText(rs.getString(1));
        Name.setText(rs.getString(2));
        Age.setText(rs.getString(3));
        gendar.setText(rs.getString(4));
        Mobile.setText(rs.getString(5));
        Address.setText(rs.getString(6));
        D_Name.setText(rs.getString(7));
        date.setText(rs.getString(8));
        time.setText(rs.getString(9));
        TotalTk.setText(rs.getString(10));
        Discount.setText(rs.getString(11));
        paid.setText(rs.getString(12));
        duetk.setText(rs.getString(13));
        test.setText(rs.getString(14));
       }
       } catch (SQLException ex) {
           Logger.getLogger(Customar_PROFILE.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
    }//GEN-LAST:event_rSButtonRiple1ActionPerformed
   public void Show(){ 
       if(Search.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "Entry Serial No or Number or Name !");
            Search.requestFocus();
       }else{
       con();
       st=null;
       try {
       String x=Search.getText();
       String m="Select `Id`,`Customar_Name`,`Age`,`Gendar`,`Mobile`,`Address`,`Dr_Name`,`Date`,`Time`,`taka`,`DisCount`,`paid`,`unpaid`,`Tests`FROM `customar` WHERE `Id`='"+x+"'or`Customar_Name`='"+x+"'or`Mobile`='"+x+"'";
       st=con.prepareCall(m);
       rs=st.executeQuery();
       while(rs.next()){
        ID.setText(rs.getString(1));
        Name.setText(rs.getString(2));
        Age.setText(rs.getString(3));
        gendar.setText(rs.getString(4));
        Mobile.setText(rs.getString(5));
        Address.setText(rs.getString(6));
        D_Name.setText(rs.getString(7));
        date.setText(rs.getString(8));
        time.setText(rs.getString(9));
        TotalTk.setText(rs.getString(10));
        Discount.setText(rs.getString(11));
        paid.setText(rs.getString(12));
        duetk.setText(rs.getString(13));
        test.setText(rs.getString(14));
       }
       } catch (SQLException ex) {
           Logger.getLogger(Customar_PROFILE.class.getName()).log(Level.SEVERE, null, ex);
       }
   }}
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Customar_PROFILE.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Customar_PROFILE.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Customar_PROFILE.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Customar_PROFILE.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Customar_PROFILE().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Address;
    private javax.swing.JLabel Age;
    private javax.swing.JLabel D_Name;
    private javax.swing.JLabel Discount;
    private javax.swing.JLabel ID;
    private javax.swing.JLabel Mobile;
    private javax.swing.JLabel Name;
    private javax.swing.JTextField Search;
    private javax.swing.JLabel TotalTk;
    private javax.swing.JLabel date;
    private javax.swing.JLabel duetk;
    private javax.swing.JLabel gendar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel paid;
    private javax.swing.JTextArea test;
    private javax.swing.JLabel time;
    // End of variables declaration//GEN-END:variables
}
