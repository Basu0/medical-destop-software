/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amin_ullah_medical;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Basu dev
 */
public class CustomarUpdate extends javax.swing.JFrame {
    Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    public CustomarUpdate() {
        initComponents();
        tableshow();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        Seril_no = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        Name = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        Contac = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        address1 = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        Drname = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(102, 0, 255));
        jPanel1.setLayout(null);

        jTable1.setFont(new java.awt.Font("Perpetua Titling MT", 1, 12)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                " NO", "Name", "Mobile", "Address", "Dr.Name", "Date", "Time", "Total Taka", "Discount", "Paid", "Due"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setIntercellSpacing(new java.awt.Dimension(3, 1));
        jTable1.setRowHeight(30);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(10, 70, 1360, 420);

        jPanel2.setBackground(new java.awt.Color(0, 0, 128));
        jPanel2.setLayout(null);

        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Minimize_Window_35px.png"))); // NOI18N
        jLabel27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel27MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel27);
        jLabel27.setBounds(1250, 10, 40, 40);

        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Close_Window_35px_1.png"))); // NOI18N
        jLabel28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel28MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel28);
        jLabel28.setBounds(1300, 10, 35, 40);

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/nn 60 pn.png"))); // NOI18N
        jPanel2.add(jLabel14);
        jLabel14.setBounds(20, 0, 60, 50);

        jLabel32.setBackground(new java.awt.Color(51, 102, 255));
        jLabel32.setFont(new java.awt.Font("SutonnyMJ", 1, 48)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(218, 87, 69));
        jLabel32.setText("Avwgb Djøvn& †gwW‡Kj †m›Uvi ");
        jPanel2.add(jLabel32);
        jLabel32.setBounds(580, 0, 500, 50);

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/nn 60 pn.png"))); // NOI18N
        jPanel2.add(jLabel15);
        jLabel15.setBounds(20, 0, 60, 50);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 1360, 50);

        jLabel20.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Serial  No :");
        jPanel1.add(jLabel20);
        jLabel20.setBounds(10, 510, 100, 40);

        Seril_no.setEditable(false);
        Seril_no.setBackground(new java.awt.Color(255, 255, 51));
        Seril_no.setFont(new java.awt.Font("Perpetua Titling MT", 1, 18)); // NOI18N
        Seril_no.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Seril_noActionPerformed(evt);
            }
        });
        jPanel1.add(Seril_no);
        Seril_no.setBounds(110, 510, 100, 40);

        jLabel19.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Name :");
        jPanel1.add(jLabel19);
        jLabel19.setBounds(230, 510, 80, 40);

        Name.setBackground(new java.awt.Color(255, 255, 51));
        Name.setFont(new java.awt.Font("Perpetua Titling MT", 1, 18)); // NOI18N
        Name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NameKeyPressed(evt);
            }
        });
        jPanel1.add(Name);
        Name.setBounds(300, 510, 520, 40);

        jLabel22.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Contract :");
        jPanel1.add(jLabel22);
        jLabel22.setBounds(720, 580, 100, 40);

        Contac.setBackground(new java.awt.Color(255, 255, 51));
        Contac.setFont(new java.awt.Font("Perpetua Titling MT", 1, 18)); // NOI18N
        Contac.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ContacKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ContacKeyTyped(evt);
            }
        });
        jPanel1.add(Contac);
        Contac.setBounds(820, 580, 260, 40);

        jLabel21.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Address  :");
        jPanel1.add(jLabel21);
        jLabel21.setBounds(840, 510, 90, 40);

        address1.setBackground(new java.awt.Color(255, 255, 51));
        address1.setFont(new java.awt.Font("Perpetua Titling MT", 1, 18)); // NOI18N
        address1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                address1KeyPressed(evt);
            }
        });
        jPanel1.add(address1);
        address1.setBounds(930, 510, 410, 40);

        jLabel24.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("DR.Name  :");
        jPanel1.add(jLabel24);
        jLabel24.setBounds(20, 580, 90, 40);

        Drname.setBackground(new java.awt.Color(255, 255, 51));
        Drname.setFont(new java.awt.Font("Perpetua Titling MT", 1, 18)); // NOI18N
        Drname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DrnameKeyPressed(evt);
            }
        });
        jPanel1.add(Drname);
        Drname.setBounds(110, 580, 590, 40);

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("Update");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(1100, 580, 110, 40);

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton2.setText("Clear");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(1230, 580, 100, 40);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(-10, 0, 1370, 670);

        setSize(new java.awt.Dimension(1382, 710));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    public void con(){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/amin_ulla", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(Amin_seceond.class.getName()).log(Level.SEVERE, null, ex);
        }}
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        DefaultTableModel ta=(DefaultTableModel)jTable1.getModel();
        Seril_no.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 0).toString());
        Name.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 1).toString());
        Contac.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 2).toString());
        address1.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 3).toString());
        Drname.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 4).toString());
       

    }//GEN-LAST:event_jTable1MouseClicked

    private void jLabel27MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel27MouseClicked
        // TODO add your handling code here:
        this.setState(JFrame.ICONIFIED);

    }//GEN-LAST:event_jLabel27MouseClicked

    private void jLabel28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel28MouseClicked
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"Exit Apps ");
        if(result==0){
            dispose();
        }
    }//GEN-LAST:event_jLabel28MouseClicked

    private void Seril_noActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Seril_noActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Seril_noActionPerformed

    private void NameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NameKeyPressed
        // TODO add your handling code here:

    }//GEN-LAST:event_NameKeyPressed

    private void ContacKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ContacKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ContacKeyPressed

    private void ContacKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ContacKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_ContacKeyTyped

    private void address1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_address1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_address1KeyPressed

    private void DrnameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DrnameKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DrnameKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      update(); 
      tableshow();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        Seril_no.setText("");
        Name.setText("");
        address1.setText("");
        Drname.setText("");
        Contac.setText("");
        
        
    }//GEN-LAST:event_jButton2ActionPerformed

    public void tableshow(){
    try { int a=0,b=0,c=0,d=0,e=0,f=0,g=0;
           con();
           DefaultTableModel ta =(DefaultTableModel)jTable1.getModel();
           while(ta.getRowCount()>0){
        for (int i = 0; i <ta.getRowCount(); i++) {
            ta.removeRow(i);
        }}
           String m="Select `Id`,`Customar_Name`,`Mobile`,`Address`,`Dr_Name`,`Date`,`Time`,`taka`,`DisCount`,`paid`,`unpaid`FROM `customar` WHERE `paid`";
           st=con.prepareCall(m);
           rs=st.executeQuery();
           while(rs.next()){
           ta.addRow(new Object[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11)});
           }
       } catch (SQLException ex) {
           Logger.getLogger(CustomarEdit.class.getName()).log(Level.SEVERE, null, ex);
       }
    
    }
    public void update(){
    try {
            st=null;
            con();
            String x=Seril_no.getText();
            String u="UPDATE `customar` SET `Customar_Name`=?,`Address`=?,`Dr_Name`=?,`Mobile`=?  WHERE `Id` ='"+x+"'";
            st=con.prepareCall(u);
            st.setString(1,Name.getText());
            st.setString(2,address1.getText());
            st.setString(3,Drname.getText());
            st.setString(4,Contac.getText());
            
            //JOptionPane.showMessageDialog(this,"Sucess Full Update");
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CustomarUpdate.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CustomarUpdate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CustomarUpdate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CustomarUpdate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CustomarUpdate.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CustomarUpdate().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Contac;
    private javax.swing.JTextField Drname;
    private javax.swing.JTextField Name;
    private javax.swing.JTextField Seril_no;
    private javax.swing.JTextField address1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
