/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amin_ullah_medical;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
public class Print2 extends javax.swing.JFrame {
Connection con=null;
    PreparedStatement st=null;
    ResultSet rs=null;
    public Print2() {
        initComponents();
        jTable1.getTableHeader().setFont(new Font("Tahamo",Font.BOLD,12));
        
        add();
        this.setResizable(false);
    }

    public void con(){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/amin_ulla", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(PrintPage.class.getName()).log(Level.SEVERE, null, ex);
        }}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        time_s = new javax.swing.JLabel();
        Date_s = new javax.swing.JLabel();
        id = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Name = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        mobile_no = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        Dr_names = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        totalcs = new javax.swing.JLabel();
        discounds = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        paids = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        unpaids = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(null);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Invoice No:");
        jPanel5.add(jLabel4);
        jLabel4.setBounds(10, 120, 70, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Date :");
        jPanel5.add(jLabel6);
        jLabel6.setBounds(320, 120, 50, 30);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setText("..................................................................................................................................................................");
        jPanel5.add(jLabel14);
        jLabel14.setBounds(0, 140, 650, 10);

        time_s.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jPanel5.add(time_s);
        time_s.setBounds(510, 120, 130, 30);

        Date_s.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jPanel5.add(Date_s);
        Date_s.setBounds(370, 120, 90, 30);

        id.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel5.add(id);
        id.setBounds(90, 120, 130, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Time :");
        jPanel5.add(jLabel3);
        jLabel3.setBounds(470, 120, 50, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText(" Name      :");
        jPanel5.add(jLabel5);
        jLabel5.setBounds(10, 150, 70, 30);

        Name.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        jPanel5.add(Name);
        Name.setBounds(70, 150, 180, 30);

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setText("Mobile    :");
        jPanel5.add(jLabel26);
        jLabel26.setBounds(320, 150, 80, 30);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText(" Money Receipt");
        jPanel5.add(jLabel12);
        jLabel12.setBounds(250, 90, 110, 20);

        mobile_no.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel5.add(mobile_no);
        mobile_no.setBounds(390, 150, 190, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Ref .By    :");
        jPanel5.add(jLabel7);
        jLabel7.setBounds(10, 190, 70, 20);

        Dr_names.setFont(new java.awt.Font("Perpetua Titling MT", 1, 12)); // NOI18N
        jPanel5.add(Dr_names);
        Dr_names.setBounds(70, 190, 550, 30);

        jTable1.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Test Name", "Taka"
            }
        ));
        jTable1.setAlignmentX(1.0F);
        jTable1.setAlignmentY(1.0F);
        jTable1.setIntercellSpacing(new java.awt.Dimension(2, 1));
        jScrollPane1.setViewportView(jTable1);

        jPanel5.add(jScrollPane1);
        jScrollPane1.setBounds(10, 220, 650, 290);

        jLabel2.setFont(new java.awt.Font("SutonnyMJ", 1, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(248, 107, 16));
        jLabel2.setText("Kzwgjøv evm ÷¨vÛ,‡dbx");
        jPanel5.add(jLabel2);
        jLabel2.setBounds(280, 40, 130, 20);

        jLabel11.setFont(new java.awt.Font("SutonnyMJ", 1, 14)); // NOI18N
        jLabel11.setText("‡gvevBj t 01829-561588, 01711-665627");
        jPanel5.add(jLabel11);
        jLabel11.setBounds(230, 60, 220, 20);

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/nn 75 pn.png"))); // NOI18N
        jPanel5.add(jLabel13);
        jLabel13.setBounds(30, 0, 90, 77);

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel22.setText("..................................................................................................................................................................");
        jPanel5.add(jLabel22);
        jLabel22.setBounds(0, 110, 648, 10);

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setText("..................................................................................................................................................................");
        jPanel5.add(jLabel15);
        jLabel15.setBounds(0, 80, 650, 10);

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setText("Total Charge :");
        jPanel5.add(jLabel17);
        jLabel17.setBounds(410, 510, 100, 30);

        totalcs.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel5.add(totalcs);
        totalcs.setBounds(520, 510, 80, 30);

        discounds.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel5.add(discounds);
        discounds.setBounds(520, 540, 80, 20);

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setText("Discount        :");
        jPanel5.add(jLabel18);
        jLabel18.setBounds(410, 540, 100, 20);

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setText("Paid               :");
        jPanel5.add(jLabel19);
        jLabel19.setBounds(410, 570, 100, 20);

        paids.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel5.add(paids);
        paids.setBounds(520, 570, 80, 20);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setText("........................................");
        jPanel5.add(jLabel9);
        jLabel9.setBounds(400, 590, 240, 14);

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel21.setText("Balance         :");
        jPanel5.add(jLabel21);
        jLabel21.setBounds(410, 610, 100, 20);

        unpaids.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel5.add(unpaids);
        unpaids.setBounds(520, 610, 80, 20);

        jLabel20.setFont(new java.awt.Font("Segoe UI Historic", 1, 14)); // NOI18N
        jLabel20.setText("THANKE YOU VISIT AGAIN");
        jPanel5.add(jLabel20);
        jLabel20.setBounds(0, 590, 200, 40);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Signature");
        jPanel5.add(jLabel16);
        jLabel16.setBounds(20, 580, 60, 20);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setText("........................................");
        jPanel5.add(jLabel8);
        jLabel8.setBounds(0, 570, 120, 14);

        jLabel23.setBackground(new java.awt.Color(51, 102, 255));
        jLabel23.setFont(new java.awt.Font("SutonnyMJ", 1, 42)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(218, 87, 69));
        jLabel23.setText("Avwgb Djøvn& †gwW‡Kj †m›Uvi ");
        jPanel5.add(jLabel23);
        jLabel23.setBounds(140, -10, 440, 60);

        getContentPane().add(jPanel5);
        jPanel5.setBounds(0, 0, 640, 670);

        jButton2.setBackground(new java.awt.Color(0, 255, 0));
        jButton2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("Print");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(260, 680, 90, 30);

        setSize(new java.awt.Dimension(655, 771));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        PrinterJob job= PrinterJob.getPrinterJob();
        job.setJobName("Print Data");
        job.setPrintable(new Printable(){
        public int print(Graphics pg,PageFormat pf,int PageNum){
        if(PageNum>0){
        return Printable.NO_SUCH_PAGE;
        }
        Graphics2D g2=(Graphics2D) pg;
        g2.translate(pf.getImageableX(),pf.getImageableY());
        g2.scale(1,1);
        jPanel5.print(g2);
        return Printable.PAGE_EXISTS;
        }
        });
        boolean print=job.printDialog();
        if(print){
          JOptionPane.showMessageDialog(this,"Print Success");try{
        job.print();
        }
        catch(PrinterException ex){} }
        else{JOptionPane.showMessageDialog(this,"Printing Faild");}
    }//GEN-LAST:event_jButton2ActionPerformed

   public void add(){
  con();
  st=null;
  CustomarEdit am=new CustomarEdit();
  String p=CustomarEdit.i;
  //String p="";
  //JOptionPane.showMessageDialog(this, " Success Full... "+p);
  DefaultTableModel ta =(DefaultTableModel)jTable1.getModel();
  String Q="SELECT `Id`,`Customar_Name`,`Mobile`,`Dr_Name`,`Date`,`Time`,`taka`,`DisCount`,`paid`,`unpaid`,`Tests`,`taka` FROM `customar`WHERE `Id`='"+p+"'";
        try {
            st=con.prepareCall(Q);
            rs=st.executeQuery();
            while(rs.next()){
            id.setText(rs.getString(1));
            Name.setText(rs.getString(2));
            mobile_no.setText(rs.getString(3));
            Dr_names.setText(rs.getString(4));
            Date_s.setText(rs.getString(5));
            time_s.setText(rs.getString(6));
            totalcs.setText(rs.getString(7));
            discounds.setText(rs.getString(8));
            paids.setText(rs.getString(9));
            unpaids.setText(rs.getString(10));
            /*String mm[]=split(",");
                for (int i = 0; i < mm.length; i++) {
              JOptionPane.showMessageDialog(this, mm[i]);
                    ta.addRow(new Object[]{mm[i],rs.getString(12)});
                }*/
            ta.addRow(new Object[]{rs.getString(11),rs.getString(12)});
            }
        } catch (SQLException ex) {
            Logger.getLogger(PrintPage.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Print2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Print2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Print2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Print2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Print2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date_s;
    private javax.swing.JLabel Dr_names;
    private javax.swing.JLabel Name;
    private javax.swing.JLabel discounds;
    private javax.swing.JLabel id;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable jTable1;
    private javax.swing.JLabel mobile_no;
    private javax.swing.JLabel paids;
    private javax.swing.JLabel time_s;
    private javax.swing.JLabel totalcs;
    private javax.swing.JLabel unpaids;
    // End of variables declaration//GEN-END:variables
}
