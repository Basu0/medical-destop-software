/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amin_ullah_medical;

import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Basu dev
 */
public class Daliy_Report extends javax.swing.JFrame {
    Connection con=null;
    PreparedStatement st=null;
    ResultSet rs=null;
    static int s1=0;
    public Daliy_Report() {
        initComponents();
        this.setResizable(false);
        tableShow();
       AutoNumberGenarator();
        jTable1.getTableHeader().setFont(new Font("Tahamo",Font.BOLD,12));
        TableColumnModel colum=jTable1.getColumnModel();
        colum.getColumn(0).setPreferredWidth(5);
        colum.getColumn(1).setPreferredWidth(300);
        colum.getColumn(2).setPreferredWidth(50);
         colum.getColumn(3).setPreferredWidth(50);
         colum.getColumn(4).setPreferredWidth(200);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    public void con(){
         try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/amin_ulla", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(Amin_seceond.class.getName()).log(Level.SEVERE, null, ex);
        }}
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        Si_no = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        particular = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        taka = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        details = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        TotalCost = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        Daliycost = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel2.setBackground(new java.awt.Color(0, 128, 128));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(null);

        jLabel32.setBackground(new java.awt.Color(51, 102, 255));
        jLabel32.setFont(new java.awt.Font("SutonnyMJ", 1, 48)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(218, 87, 69));
        jLabel32.setText("Avwgb Djøvn& †gwW‡Kj †m›Uvi ");
        jPanel2.add(jLabel32);
        jLabel32.setBounds(170, 0, 500, 50);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 820, 60);

        jPanel1.setBackground(new java.awt.Color(0, 128, 128));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("DATE :");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(380, 60, 50, 30);

        Si_no.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(Si_no);
        Si_no.setBounds(60, 10, 90, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("PARTICULARS :");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(150, 10, 100, 30);

        particular.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(particular);
        particular.setBounds(250, 10, 410, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("TAKA :");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(670, 10, 50, 30);

        taka.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        taka.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                takaKeyTyped(evt);
            }
        });
        jPanel1.add(taka);
        taka.setBounds(720, 10, 80, 30);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("No     :");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 10, 50, 30);
        jPanel1.add(jDateChooser1);
        jDateChooser1.setBounds(430, 60, 170, 30);

        details.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(details);
        details.setBounds(80, 60, 290, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("DETAILS :");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 60, 70, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Total Cost :");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(240, 100, 80, 40);

        TotalCost.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TotalCost.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(TotalCost);
        TotalCost.setBounds(320, 100, 70, 40);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Daliy Cost :");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(420, 100, 80, 40);

        Daliycost.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Daliycost.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(Daliycost);
        Daliycost.setBounds(500, 100, 70, 40);

        jButton1.setText("Delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(710, 60, 80, 30);

        jButton2.setText("Submit");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(610, 60, 90, 30);

        jButton3.setText("Total Cost");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(120, 100, 100, 40);

        jButton4.setText("Clear");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);
        jButton4.setBounds(710, 100, 80, 30);

        jButton5.setText("Today");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5);
        jButton5.setBounds(610, 100, 90, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 60, 820, 150);

        jTable1.setFont(new java.awt.Font("SimSun", 0, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No", "PARTICULARS ", "Taka", "Date", "Details"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(0, 210, 820, 320);

        setSize(new java.awt.Dimension(832, 552));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void rSButtonRiple1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSButtonRiple1ActionPerformed
        // TODO add your handling code here:
        daliyaddd();
        tableShow();
        Si_no.setText("");
        particular.setText("");
        taka.setText("");
        details.setText("");
        AutoNumberGenarator();
    }//GEN-LAST:event_rSButtonRiple1ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        DefaultTableModel ta=(DefaultTableModel)jTable1.getModel();
        Si_no.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 0).toString());
        particular.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 1).toString());
        taka.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 2).toString());
        details.setText(""+ta.getValueAt(jTable1.getSelectedRow(), 3).toString());
        
    }//GEN-LAST:event_jTable1MouseClicked

    private void rSButtonRiple2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSButtonRiple2ActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_rSButtonRiple2ActionPerformed

    private void rSButtonRiple3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSButtonRiple3ActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_rSButtonRiple3ActionPerformed

    private void rSButtonRiple4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSButtonRiple4ActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_rSButtonRiple4ActionPerformed

    private void takaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_takaKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
        if(!(Character.isDigit(c) || c==KeyEvent.VK_SPACE || c==KeyEvent.VK_DELETE)){
            evt.consume();
        }
    }//GEN-LAST:event_takaKeyTyped

    private void rSButtonRiple5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSButtonRiple5ActionPerformed
        // TODO add your handling code here:
        tableShow();
    }//GEN-LAST:event_rSButtonRiple5ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        daliyaddd();
        tableShow();
         Si_no.setText("");
        particular.setText("");
        taka.setText("");
        details.setText("");
        AutoNumberGenarator();
       
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"You Sure Delete ","Cost", JOptionPane.WARNING_MESSAGE);
        if(result==0){
        if(Si_no.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "Entry Id No !");
       }else{
        try {
            con();
            String x=Si_no.getText();
            //int x=Integer.parseInt(Seril_no.getText().toString());
           String u="DELETE FROM `daliyreport` WHERE `id` ='"+x+"'";
            st=con.prepareCall(u);
            st.executeUpdate();
          DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
       while(dm.getRowCount()>0){
        for (int i = 0; i <dm.getRowCount(); i++) {
            dm.removeRow(i);
        }}
       tableShow();
        Si_no.setText("");
        particular.setText("");
        taka.setText("");
        details.setText("");
        } catch (SQLException ex) {
            Logger.getLogger(Insart_data.class.getName()).log(Level.SEVERE, null, ex);
        }}}
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        try { int a=0;
            DefaultTableModel ta =(DefaultTableModel)jTable1.getModel();
            
           while(ta.getRowCount()>0){
           for (int i = 0; i <ta.getRowCount(); i++) {
            ta.removeRow(i);
           }}
            con();
           Date date = new Date();
           SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           String n = sdf.format(date);
            String Q="SELECT `id`,`partucalr`,`Taka`,`Date`,`Datials`FROM `daliyreport` WHERE `Date`='"+n+"'";
            st=con.prepareCall(Q);
            rs=st.executeQuery();
            while(rs.next()){
                a=a+rs.getInt(3);
               Daliycost.setText(""+a);
                ta.addRow(new Object[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5)});
            }
        } catch (SQLException ex) {
            Logger.getLogger(Daliy_Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        Si_no.setText("");
        particular.setText("");
        taka.setText("");
        details.setText("");
        AutoNumberGenarator();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        tableShow();
    }//GEN-LAST:event_jButton3ActionPerformed
   public void daliyaddd(){
        try {
            con();
            
           SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           String date=sdf.format(jDateChooser1.getDate()).toString();
            String q="INSERT INTO `daliyreport`(`id`,`partucalr`,`Taka`,`Date`,`Datials`) VALUES (?,?,?,?,?)";
            st=con.prepareCall(q);
            st.setString(1,Si_no.getText());
            st.setString(2,particular.getText());
            st.setString(3,taka.getText());
            st.setString(4,date);
            st.setString(5,details.getText());
             st.executeUpdate();
             JOptionPane.showMessageDialog(this, " Success Full... ");
            
        } catch (SQLException ex) {
            Logger.getLogger(Daliy_Report.class.getName()).log(Level.SEVERE, null, ex);
        }
 
 }
    public void tableShow(){
        try { int a=0;
            DefaultTableModel ta =(DefaultTableModel)jTable1.getModel();
            
           while(ta.getRowCount()>0){
           for (int i = 0; i <ta.getRowCount(); i++) {
            ta.removeRow(i);
           }}
            con();
            String Q="SELECT `id`,`partucalr`,`Taka`,`Date`,`Datials` FROM `daliyreport`";
            st=con.prepareCall(Q);
            rs=st.executeQuery();
            while(rs.next()){
                a=a+rs.getInt(3);
               TotalCost.setText(""+a);
                ta.addRow(new Object[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5)});
            }
        } catch (SQLException ex) {
            Logger.getLogger(Daliy_Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    public void AutoNumberGenarator(){
        try {
            con();
            String R="SELECT `id` FROM `daliyreport` ";
            st=con.prepareCall(R);
            rs=st.executeQuery();
        while(rs.next()){
             s1=rs.getInt(1);
                //JOptionPane.showMessageDialog(this,"Already Seril Add!!","Try Agin!!",JOptionPane.WARNING_MESSAGE); 
            }
          s1++;
          Si_no.setText(""+s1);
            Random rr=new Random();
            /*int n1=5+rr.nextInt();
            Seril_no.setText("A"+n1);
            int n2=10+rr.nextInt(20);
            Seril_no.setText(""+n2);
            int n3=500*2+rr.nextInt(35);
            Seril_no.setText(""+n3);
            int n4=25-rr.nextInt();
            Seril_no.setText("A"+n4);
            int n5=rr.nextInt(25);
            Seril_no.setText(""+n5);
            int n6=125*2/rr.nextInt(30);
            Seril_no.setText(""+n6);*/
            
           
            particular.requestFocus();
            Date da=new Date();
        } catch (SQLException ex) {
            Logger.getLogger(Daliy_Report.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
              }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Daliy_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Daliy_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Daliy_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Daliy_Report.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Daliy_Report().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Daliycost;
    private javax.swing.JTextField Si_no;
    private javax.swing.JLabel TotalCost;
    private javax.swing.JTextField details;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField particular;
    private javax.swing.JTextField taka;
    // End of variables declaration//GEN-END:variables
}
