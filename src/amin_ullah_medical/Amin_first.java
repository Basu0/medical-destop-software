
package amin_ullah_medical;

import com.sun.glass.events.KeyEvent;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.jar.Attributes.Name;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import sun.security.util.Password;

/**
 *
 * @author Basu dev
 */
public class Amin_first extends javax.swing.JFrame {
Connection con = null;
    PreparedStatement st = null;
    ResultSet rs = null;
    static String u=null;
    static String p=null;
    public Amin_first() {
        //this.setResizable(false);
        //this.setVisible(true);
        initComponents();
       
        
    }

  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        user = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPasswordField1 = new javax.swing.JPasswordField();
        jButton1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel5.setLayout(null);

        jSeparator1.setForeground(new java.awt.Color(153, 153, 153));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel5.add(jSeparator1);
        jSeparator1.setBounds(90, 200, 216, 2);
        jPanel5.add(jLabel2);
        jLabel2.setBounds(0, 0, 0, 0);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 102, 0));
        jLabel3.setText("User:");
        jPanel5.add(jLabel3);
        jLabel3.setBounds(90, 130, 54, 22);

        user.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        user.setForeground(new java.awt.Color(204, 204, 204));
        user.setText("User Name");
        user.setBorder(null);
        user.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                userMouseClicked(evt);
            }
        });
        user.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userActionPerformed(evt);
            }
        });
        user.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                userKeyPressed(evt);
            }
        });
        jPanel5.add(user);
        user.setBounds(120, 160, 188, 31);
        jPanel5.add(jLabel4);
        jLabel4.setBounds(0, 0, 0, 0);

        jSeparator2.setForeground(new java.awt.Color(153, 153, 153));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel5.add(jSeparator2);
        jSeparator2.setBounds(90, 290, 216, 2);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 102, 0));
        jLabel5.setText("Password:");
        jPanel5.add(jLabel5);
        jLabel5.setBounds(90, 220, 80, 22);
        jPanel5.add(jLabel6);
        jLabel6.setBounds(0, 0, 0, 0);

        jPasswordField1.setForeground(new java.awt.Color(204, 204, 204));
        jPasswordField1.setText("jPasswordField1");
        jPasswordField1.setBorder(null);
        jPasswordField1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPasswordField1MouseClicked(evt);
            }
        });
        jPasswordField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPasswordField1ActionPerformed(evt);
            }
        });
        jPasswordField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPasswordField1KeyPressed(evt);
            }
        });
        jPanel5.add(jPasswordField1);
        jPasswordField1.setBounds(120, 250, 188, 31);

        jButton1.setBackground(new java.awt.Color(255, 153, 51));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Login ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton1);
        jButton1.setBounds(130, 320, 103, 33);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/IMG 75.png"))); // NOI18N
        jPanel5.add(jLabel7);
        jLabel7.setBounds(160, 50, 75, 80);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/profile.png"))); // NOI18N
        jPanel5.add(jLabel8);
        jLabel8.setBounds(90, 160, 24, 24);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/key-black-silhouette-interface-symbol-for-login (1).png"))); // NOI18N
        jPanel5.add(jLabel9);
        jLabel9.setBounds(90, 250, 24, 24);

        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Minimize_Window_35px.png"))); // NOI18N
        jLabel27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel27MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel27);
        jLabel27.setBounds(1250, 10, 40, 40);

        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Close_Window_35px_1.png"))); // NOI18N
        jLabel28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel28MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel28);
        jLabel28.setBounds(1300, 10, 35, 40);

        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Minimize_Window_35px.png"))); // NOI18N
        jLabel29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel29MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel29);
        jLabel29.setBounds(1250, 10, 40, 40);

        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/icons8_Close_Window_35px_1.png"))); // NOI18N
        jLabel30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel30MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel30);
        jLabel30.setBounds(1300, 10, 35, 40);

        getContentPane().add(jPanel5);
        jPanel5.setBounds(0, 0, 370, 420);

        setSize(new java.awt.Dimension(382, 456));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
public void con() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/amin_ulla", "root", "");
        } catch (SQLException ex) {
            Logger.getLogger(Amin_first.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void userMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_userMouseClicked
        // TODO add your handling code here:
        user.setText("");
    }//GEN-LAST:event_userMouseClicked

    private void jPasswordField1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPasswordField1MouseClicked
        // TODO add your handling code here:
        jPasswordField1.setText("");
    }//GEN-LAST:event_jPasswordField1MouseClicked

    private void jPasswordField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPasswordField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jPasswordField1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        con();
        u=user.getText();
        p=jPasswordField1.getText();
        if(u.isEmpty()){
            JOptionPane.showMessageDialog(this,"User is Empty!!");
            user.requestFocus();
        }else if (p.isEmpty()){
            JOptionPane.showMessageDialog(this,"Pass is Empty!!");
            jPasswordField1.requestFocus();
        }
        else{
            try {
                String m="SELECT `User_Name`,`Password` FROM  `sequrity` WHERE `User_Name`='"+u+"'And `Password`='"+p+"'";
                st=con.prepareCall(m);
                rs=st.executeQuery();
                if(rs.next()){
                    JOptionPane.showMessageDialog(this,"Login Success");
                    Insart_data bb=new Insart_data();
                    bb.setVisible(true);
                }else{
                 JOptionPane.showConfirmDialog(null,"User Name And Password Wrong","Please", JOptionPane.ERROR_MESSAGE);
            }}
            catch (SQLException ex) {
                Logger.getLogger(Amin_first.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //JOptionPane.showMessageDialog(this,"Login Faild");
    }//GEN-LAST:event_jButton1ActionPerformed

    private void userActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_userActionPerformed

    private void userKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_userKeyPressed
        // TODO add your handling code here:
        //user.setText("");
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        jPasswordField1.requestFocus();
        jPasswordField1.setText("");
        }
    }//GEN-LAST:event_userKeyPressed

    private void jPasswordField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPasswordField1KeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
        {con();
        u=user.getText();
        p=jPasswordField1.getText();
        if(u.isEmpty()){
            JOptionPane.showMessageDialog(this,"User is Empty!!");
            user.requestFocus();
        }else if (p.isEmpty()){
            JOptionPane.showMessageDialog(this,"Pass is Empty!!");
            jPasswordField1.requestFocus();
        }
        else{
            try {
                String m="SELECT `User_Name`,`Password` FROM  `sequrity` WHERE `User_Name`='"+u+"'And `Password`='"+p+"'";
                st=con.prepareCall(m);
                rs=st.executeQuery();
                if(rs.next()){
                    JOptionPane.showMessageDialog(this,"Login Success");
                    Insart_data bb=new Insart_data();
                    bb.setVisible(true);
                }else{
                 JOptionPane.showConfirmDialog(null,"User Name And Password Wrong","Please", JOptionPane.ERROR_MESSAGE);
            }}
            catch (SQLException ex) {
                Logger.getLogger(Amin_first.class.getName()).log(Level.SEVERE, null, ex);
            }
        }}
    }//GEN-LAST:event_jPasswordField1KeyPressed

    private void jLabel27MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel27MouseClicked
        // TODO add your handling code here:
        this.setState(JFrame.ICONIFIED);
    }//GEN-LAST:event_jLabel27MouseClicked

    private void jLabel28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel28MouseClicked
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"Exit Apps ");
        if(result==0){
            dispose();
        }
    }//GEN-LAST:event_jLabel28MouseClicked

    private void jLabel29MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel29MouseClicked
        // TODO add your handling code here:
        this.setState(JFrame.ICONIFIED);
    }//GEN-LAST:event_jLabel29MouseClicked

    private void jLabel30MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel30MouseClicked
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"Exit Apps ");
        if(result==0){
            dispose();
        }
    }//GEN-LAST:event_jLabel30MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Amin_first.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Amin_first.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Amin_first.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Amin_first.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Amin_first().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField user;
    // End of variables declaration//GEN-END:variables

   

    private void setIcon() {
      // setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Logo.jpg"))); 
    }
}
