/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amin_ullah_medical;
//package  swing_6;

import com.sun.glass.events.KeyEvent;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Basu dev
 */
public class Doctor extends javax.swing.JFrame {
    Connection con=null;
    PreparedStatement st=null;
    ResultSet rs=null;
    
    public Doctor() {
        initComponents();
         jTable1.getTableHeader().setFont(new Font("Tahamo",Font.BOLD,12));
        tableshow();
     Toolkit tk=Toolkit.getDefaultToolkit();
     //this.setIconImage(tk.getImage(getClass().getResource("utc1.png")));

    }

    public void con(){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/amin_ulla", "root", "");
        } catch (SQLException ex) {
JOptionPane.showMessageDialog(this, ex.getMessage(),"Server Not Found",JOptionPane.WARNING_MESSAGE);
        }}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        Dr_name = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        AddressDr = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        Mobile_no = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        Attendanttime = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        Attendantday = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(68, 10, 127));
        jPanel2.setLayout(null);

        jLabel22.setBackground(new java.awt.Color(51, 102, 255));
        jLabel22.setFont(new java.awt.Font("SutonnyMJ", 1, 48)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(218, 87, 69));
        jLabel22.setText("Avwgb Djøvn& †gwW‡Kj †m›Uvi ");
        jPanel2.add(jLabel22);
        jLabel22.setBounds(270, -10, 500, 60);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/amin_ullah_medical/Icon/nn 60 pn.png"))); // NOI18N
        jPanel2.add(jLabel3);
        jLabel3.setBounds(40, -6, 70, 60);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 940, 50);

        jPanel3.setBackground(new java.awt.Color(68, 10, 127));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        jPanel3.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Doctor Name   :");
        jPanel3.add(jLabel2);
        jLabel2.setBounds(10, 50, 110, 30);

        Dr_name.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        Dr_name.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        Dr_name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                Dr_nameKeyPressed(evt);
            }
        });
        jPanel3.add(Dr_name);
        Dr_name.setBounds(130, 50, 180, 30);

        jLabel4.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Address          :");
        jPanel3.add(jLabel4);
        jLabel4.setBounds(10, 210, 110, 30);

        AddressDr.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        AddressDr.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AddressDrKeyPressed(evt);
            }
        });
        jPanel3.add(AddressDr);
        AddressDr.setBounds(130, 210, 180, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Number          :");
        jPanel3.add(jLabel5);
        jLabel5.setBounds(10, 260, 110, 30);

        Mobile_no.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        Mobile_no.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                Mobile_noKeyTyped(evt);
            }
        });
        jPanel3.add(Mobile_no);
        Mobile_no.setBounds(130, 260, 180, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("AttendantTime :");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(0, 160, 120, 30);

        Attendanttime.setFont(new java.awt.Font("Perpetua Titling MT", 1, 14)); // NOI18N
        Attendanttime.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AttendanttimeKeyPressed(evt);
            }
        });
        jPanel3.add(Attendanttime);
        Attendanttime.setBounds(130, 160, 180, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Attendant       :");
        jPanel3.add(jLabel7);
        jLabel7.setBounds(10, 110, 110, 30);

        Attendantday.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        Attendantday.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AttendantdayKeyPressed(evt);
            }
        });
        jPanel3.add(Attendantday);
        Attendantday.setBounds(130, 110, 180, 30);

        jLabel8.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Clear");
        jLabel8.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel8MouseMoved(evt);
            }
        });
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel8MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel8MouseExited(evt);
            }
        });
        jPanel3.add(jLabel8);
        jLabel8.setBounds(250, 320, 60, 30);

        jLabel9.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Submit ");
        jLabel9.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel9MouseMoved(evt);
            }
        });
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel9MouseExited(evt);
            }
        });
        jPanel3.add(jLabel9);
        jLabel9.setBounds(30, 320, 60, 30);

        jLabel10.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Update ");
        jLabel10.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel10MouseMoved(evt);
            }
        });
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel10MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel10MouseExited(evt);
            }
        });
        jPanel3.add(jLabel10);
        jLabel10.setBounds(100, 320, 60, 30);

        jLabel11.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Delete");
        jLabel11.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel11MouseMoved(evt);
            }
        });
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel11MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel11MouseExited(evt);
            }
        });
        jPanel3.add(jLabel11);
        jLabel11.setBounds(170, 320, 60, 30);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(0, 50, 320, 370);

        jPanel4.setLayout(null);

        jTable1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jTable1.setForeground(new java.awt.Color(153, 0, 153));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Doctor Name", "Attendant ", "Attendant  Time", "Number", "Address"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel4.add(jScrollPane1);
        jScrollPane1.setBounds(0, 0, 620, 370);

        jPanel1.add(jPanel4);
        jPanel4.setBounds(320, 50, 610, 370);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 940, 420);

        setSize(new java.awt.Dimension(949, 454));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        // TODO add your handling code here:
        add();
        DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
       while(dm.getRowCount()>0){
        for (int i = 0; i <dm.getRowCount(); i++) {
            dm.removeRow(i);
        }}
        tableshow();
    }//GEN-LAST:event_jLabel9MouseClicked

    private void Dr_nameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Dr_nameKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        Attendantday.requestFocus();
        }
        
        
    }//GEN-LAST:event_Dr_nameKeyPressed

    private void AttendantdayKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AttendantdayKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        Attendanttime.requestFocus();
        }
    }//GEN-LAST:event_AttendantdayKeyPressed

    private void AttendanttimeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AttendanttimeKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        AddressDr.requestFocus();
        }
    }//GEN-LAST:event_AttendanttimeKeyPressed

    private void AddressDrKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AddressDrKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        Mobile_no.requestFocus();
        }
    }//GEN-LAST:event_AddressDrKeyPressed

    private void Mobile_noKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Mobile_noKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
         if(!(Character.isDigit(c) || c==KeyEvent.VK_SPACE || c==KeyEvent.VK_DELETE)){
         evt.consume();
         }
    }//GEN-LAST:event_Mobile_noKeyTyped

    private void jLabel9MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseMoved
        // TODO add your handling code here:
        jLabel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel9MouseMoved

    private void jLabel9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseExited
        // TODO add your handling code here:
        jLabel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    }//GEN-LAST:event_jLabel9MouseExited

    private void jLabel10MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseMoved
        // TODO add your handling code here:
        jLabel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel10MouseMoved

    private void jLabel10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseExited
        // TODO add your handling code here:
        jLabel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    }//GEN-LAST:event_jLabel10MouseExited

    private void jLabel11MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseMoved
        // TODO add your handling code here:
        jLabel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel11MouseMoved

    private void jLabel11MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseExited
        // TODO add your handling code here:
        jLabel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    }//GEN-LAST:event_jLabel11MouseExited

    private void jLabel8MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseMoved
        // TODO add your handling code here:
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(200, 200, 200)));
    }//GEN-LAST:event_jLabel8MouseMoved

    private void jLabel8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseExited
        // TODO add your handling code here:
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(68,10,127)));
    }//GEN-LAST:event_jLabel8MouseExited

    private void jLabel10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseClicked
        try {
            // TODO add your handling code here:
            con();
            String x=Dr_name.getText();
            String u="UPDATE `amin_ulla`.`doctort` SET  `Doctor_name` =?  , `Attendant` =?, `Attendant_time` =?, `mobile` =?, `Address` =? WHERE `Doctor_name` = '"+x+"'";
            st=con.prepareCall(u);
            st.setString(1,Dr_name.getText());
            st.setString(2,Attendantday.getText());
             st.setString(3,Attendanttime.getText());
              st.setString(5,AddressDr.getText());
               st.setString(4,Mobile_no.getText());
            if(st.executeUpdate()>0){
 JOptionPane.showMessageDialog(this,"Data Update Success");
            DefaultTableModel dm=(DefaultTableModel)jTable1.getModel();
        dm.setRowCount(0);
           tableshow();}
        } catch (SQLException ex) {
            Logger.getLogger(Doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_jLabel10MouseClicked

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        DefaultTableModel ta=(DefaultTableModel)jTable1.getModel();
        Dr_name.setText(ta.getValueAt(jTable1.getSelectedRow(), 0).toString());
        Attendantday.setText(ta.getValueAt(jTable1.getSelectedRow(), 1).toString());
        Attendanttime.setText(ta.getValueAt(jTable1.getSelectedRow(), 2).toString());
        AddressDr.setText(ta.getValueAt(jTable1.getSelectedRow(), 4).toString());
        Mobile_no.setText(ta.getValueAt(jTable1.getSelectedRow(), 3).toString());
    }//GEN-LAST:event_jTable1MouseClicked

    private void jLabel11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseClicked
        // TODO add your handling code here:
        int dialog=JOptionPane.YES_NO_OPTION;
        int result=JOptionPane.showConfirmDialog(null,"You Sure Delete Doctor Account","Test", JOptionPane.WARNING_MESSAGE);
        if(result==0){
        if(Dr_name.getText().isEmpty())
            {
            JOptionPane.showMessageDialog(this, "You Select Doctor Name");
       }else{
        
            try {
                con();
                String x=Dr_name.getText();
                String u="DELETE FROM `doctort` WHERE `Doctor_name` ='"+x+"'";
                st=con.prepareCall(u);
                if(st.executeUpdate()>0){
             DefaultTableModel ta =(DefaultTableModel)jTable1.getModel();
            ta.setRowCount(0);
                tableshow();
                     Dr_name.setText("");
                        Attendantday.setText("");
                        Attendanttime.setText("");
                        AddressDr.setText("");
                        Mobile_no.setText("");
                    
                }
            } catch (SQLException ex) {
                Logger.getLogger(Doctor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        }
    }//GEN-LAST:event_jLabel11MouseClicked

    private void jLabel8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseClicked
        // TODO add your handling code here:
        Dr_name.setText("");
         Attendantday.setText("");
         Attendanttime.setText("");
       AddressDr.setText("");
      Mobile_no.setText("");
    }//GEN-LAST:event_jLabel8MouseClicked
       public void add(){
        try {
            con();
            st=null;
            String q="INSERT INTO `doctort`(`Doctor_name`,`Attendant`,`Attendant_time`,`Address`,`mobile`) VALUES (?,?,?,?,?)";
            st=con.prepareCall(q);
            st.setString(1,Dr_name.getText());
            //st.setString(2,Titel_name.getText());
            st.setString(2,Attendantday.getText());
            st.setString(3,Attendanttime.getText());
            st.setString(4,AddressDr.getText());
            st.setString(5,Mobile_no.getText());
           if(st.executeUpdate()>0){
            JOptionPane.showMessageDialog(this,"SuccessFull...");
              Dr_name.setText("");
         Attendantday.setText("");
         Attendanttime.setText("");
       AddressDr.setText("");
      Mobile_no.setText("");
           } } catch (SQLException ex) {
            Logger.getLogger(Doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
       
       public void tableshow(){
        try {
            con();
            st=null;
            int p=0;
DefaultTableModel ta =(DefaultTableModel)jTable1.getModel();
            String Q="SELECT `Doctor_name`,`Attendant`,`Attendant_time`,`mobile`,`Address` FROM`doctort`";
            
            st=con.prepareCall(Q);
            rs=st.executeQuery();
            while(rs.next())
            { ta.addRow(new Object[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5)});
            
            } } catch (SQLException ex) {
            Logger.getLogger(Doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
}
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Doctor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Doctor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Doctor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Doctor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Doctor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField AddressDr;
    private javax.swing.JTextField Attendantday;
    private javax.swing.JTextField Attendanttime;
    private javax.swing.JTextField Dr_name;
    private javax.swing.JTextField Mobile_no;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
